﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(OGButton))]
public class OGButtonEditor : Editor {

    public override void OnInspectorGUI()
    {
        var o = (OGButton)target;
        if(GUILayout.Button("Debug Push"))
        {
            o.MainAction();
        }

        base.OnInspectorGUI();
    }
}
