﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(LR_PasswordColorButton))]
public class ButtonPasswordProtect : Editor {

    public override void OnInspectorGUI()
    {
        if(Application.isPlaying) {
            var o = (LR_PasswordColorButton)target;
            if (GUILayout.Button("Debug Push"))
            {
                o.MainAction();
            }
        }
        base.OnInspectorGUI();
    }
}
