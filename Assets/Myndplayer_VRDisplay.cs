﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Myndplayer_VRDisplay : MonoBehaviour {
    public DisplayData MP_Data;
    public Text _UIAttention;
    public Text _UIMeditation;
    public Text _Connection;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        _Connection.text = "Connection : " + MP_Data.PoorSignal.ToString();
        _UIAttention.text = "Attention : " + MP_Data.Attention.ToString();
        _UIMeditation.text = "Meditation : " + MP_Data.Meditation.ToString();
	}
}
