﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine.UI;

//[ExecuteInEditMode]
public class CurveMesh : MonoBehaviour
{

    public List<Transform> ControlPoints;
    protected List<Vector3> ControlPointsPositions;
    public int PointsCount;
    protected int LastPointsCount;

    protected LineRenderer LineRenderer;
    public LineRenderer ControlPointsLineRenderer;

    public bool UseCasteljau;
    public bool UseJustPoints;

    public float MeshEdgeHeight = 0.15f;
    public float MeshEdgeWidth = 0.15f;
    public float MeshRoadWidth = 1.0f;
    [Range(0.1f, 3.0f)]
    public float MeshScale = 1.0f;
    protected float LastScale;

    protected MeshFilter MeshFilterComponent;
    protected MeshCollider MeshColliderComponent;

    public Vector3[] meshPoint;

    private GameObject Begin;
    private GameObject End;
    public Material mat;

    private void Start()
    {
        Debug.Log("after");
        LineRenderer = GetComponent<LineRenderer>();
        MeshFilterComponent = GetComponent<MeshFilter>();
        MeshColliderComponent = GetComponent<MeshCollider>();

        ControlPointsPositions = new List<Vector3>();
        Generate();
        LastPointsCount = PointsCount;
        LastScale = MeshScale;
    }


    Vector3 GetPointCasteljau(Vector3[] controlPoints, float u)
    {
        Vector3[] points = new Vector3[controlPoints.Length - 1];
        for (int i = 0; i < points.Length; i++)
        {
            points[i] = Vector3.Lerp(controlPoints[i], controlPoints[i + 1], u);
        }

        if (points.Length == 1)
        {
            return points[0];
        }
        else
        {
            return GetPointCasteljau(points, u);
        }
    }

    Vector3 GetPointBernstein(float u)
    {
        Vector3 point = new Vector3(0.0f, 0.0f, 0.0f);
        for (int i = 0; i < ControlPoints.Count; i++)
        {
            float b = Bernstein_polynomial(i, ControlPoints.Count - 1, u);
            point.x += b * ControlPoints[i].position.x;
            point.y += b * ControlPoints[i].position.y;
            point.z += b * ControlPoints[i].position.z;
        }
        return point;
    }

    Vector3 GetTangent(Vector3[] controlPoints, float u)
    {
        Vector3[] points = new Vector3[controlPoints.Length - 1];
        for (int i = 0; i < points.Length; i++)
        {
            points[i] = Vector3.Lerp(controlPoints[i], controlPoints[i + 1], u);
        }

        if (points.Length == 2)
        {
            return (points[0] - points[1]).normalized;
        }
        else
        {
            return GetTangent(points, u);
        }
    }

    protected void Generate()
    {
        //Debug.Log(PointsCount);
        if (ControlPoints.Count < 3)
        {
            LineRenderer.positionCount = 0;

            ControlPointsLineRenderer.positionCount = 0;

            this.gameObject.GetComponent<MeshRenderer>().enabled = false;
            this.gameObject.GetComponent<MeshCollider>().enabled = false;

            return; ////
        } else
        {
            this.gameObject.GetComponent<MeshRenderer>().enabled = true;
            this.gameObject.GetComponent<MeshCollider>().enabled = true;
        }


        if (PointsCount <= 1)
        {
            PointsCount = 2;
        }


        if (ControlPointsPositions.Count == ControlPoints.Count)
        {
            bool controlPointsHasChanged = false;
            for (int i = 0; i < ControlPoints.Count; i++)
            {
                if (ControlPointsPositions[i] != ControlPoints[i].position)
                {
                    controlPointsHasChanged = true;
                    break;
                }
            }

            if (!controlPointsHasChanged)
            {
                if (LastPointsCount != PointsCount)
                {
                    LastPointsCount = PointsCount;
                }
                else if (MeshScale != LastScale)
                {
                    LastScale = MeshScale;
                }
                else
                {
                    return;
                }
            }
        }

        ControlPointsPositions.Clear();
        for (int i = 0; i < ControlPoints.Count; i++)
        {
            ControlPointsPositions.Add(ControlPoints[i].position);
        }
        PointsCount = ControlPoints.Count * 10;


        var watch = System.Diagnostics.Stopwatch.StartNew();

        float edgeHeight = MeshEdgeHeight * MeshScale;
        float edgeWidth = MeshEdgeWidth * MeshScale;
        float roadWidth = MeshRoadWidth * MeshScale;

        int index = 0;

        int segments = PointsCount - 1;
        int triCount = segments * 7 * 2 + 2 * segments;
        int triIndexCount = triCount * 3;

        Vector3[] vertices = new Vector3[8 * PointsCount];
        Vector3[] normals = new Vector3[8 * PointsCount];
        Vector2[] uvs = new Vector2[8 * PointsCount];
        int[] triangleIndices = new int[triIndexCount];

        Vector3[,] beginValue = new Vector3[16, 2];
        int[] beginEdges = OuterEdgesBegin();
        Vector3[,] endValue = new Vector3[16, 2];
        int[] endEdges = new int[2];


        int size = PointsCount + 2;
        float movement = 1f / size;
        float centerDisp = 0.5f / 6f;

        Vector2[] UVB = UVBegin(PointsCount),
            UBE = UVEnd(PointsCount);

        Vector3[] points = new Vector3[PointsCount];
        for (int i = 0; i < PointsCount; i++)
        {
            float u = i * (1.0f / (PointsCount - 1));
            if (UseCasteljau)
            {
                points[i] = GetPointCasteljau(ControlPointsPositions.ToArray(), u);
            }
            /*else if (UseJustPoints)
            {
                points[i] = controlPointsPositions[i];
            }*/
            else
            {
                points[i] = GetPointBernstein(u);
            }

            Vector3 tangent = GetTangent(ControlPointsPositions.ToArray(), u);
            Debug.DrawLine(points[i], points[i] + tangent, Color.red);
            Vector3 binormal = Vector3.Cross(Vector3.up, tangent).normalized;
            Debug.DrawLine(points[i], points[i] + binormal, Color.green);
            Vector3 normal = Vector3.Cross(tangent, binormal);
            Debug.DrawLine(points[i], points[i] + normal, Color.cyan);

            /*if (i == 0)
            {
                AddOuterPart(points[i], binormal, normal, tangent,1);
            } else if (i == PointsCount - 1)
            {
                AddOuterPart(points[i], binormal, normal, tangent, -1);
            }*/
            if (i == 0)
            {
                beginValue = CreateOuterPart(points[i], binormal, normal, tangent, 1);
            } else if (i == PointsCount-1)
            {
                Debug.Log("generating end");
                endValue = CreateOuterPart(points[i], binormal, normal, tangent, -1);
                endEdges = OuterEdgesEnd(PointsCount*8);
            }

            vertices[i * 8 + 0] = points[i] + binormal * (roadWidth / 2.0f + edgeWidth);
            normals[i * 8 + 0] = (binormal - normal) / 2.0f;
            uvs[i * 8 + 0] = new Vector2(1f,1f - movement * (i + 1));

            vertices[i * 8 + 1] = points[i] + binormal * (roadWidth / 2.0f + edgeWidth) + normal * (edgeHeight);
            normals[i * 8 + 1] = (binormal + normal) / 2.0f;
            uvs[i * 8 + 1] = new Vector2(0.5f + 5 * centerDisp, 1f - movement * (i + 1));

            vertices[i * 8 + 2] = points[i] + binormal * (roadWidth / 2.0f) + normal * edgeHeight;
            normals[i * 8 + 2] = (-binormal + normal) / 2.0f;
            uvs[i * 8 + 2] = new Vector2(0.5f + 4 * centerDisp, 1f - movement * (i + 1));

            vertices[i * 8 + 3] = points[i] + binormal * (roadWidth / 2.0f); //middle right
            normals[i * 8 + 3] = (-binormal + normal) / 2.0f;
            uvs[i * 8 + 3] = new Vector2(0.5f + 3 * centerDisp, 1f - movement * (i + 1));

            vertices[i * 8 + 4] = points[i] - binormal * (roadWidth / 2.0f); //middle left
            normals[i * 8 + 4] = (binormal + normal) / 2.0f;
            uvs[i * 8 + 4] = new Vector2(0.5f - 3 * centerDisp, 1f - movement * (i + 1));

            vertices[i * 8 + 5] = points[i] - binormal * (roadWidth / 2.0f) + normal * edgeHeight;
            normals[i * 8 + 5] = (binormal + normal) / 2.0f;
            uvs[i * 8 + 5] = new Vector2(0.5f - 4 * centerDisp, 1f - movement * (i + 1));

            vertices[i * 8 + 6] = points[i] - binormal * (roadWidth / 2.0f + edgeWidth) + normal * (edgeHeight);
            normals[i * 8 + 6] = (-binormal + normal) / 2.0f;
            uvs[i * 8 + 6] = new Vector2(0.5f - 5 * centerDisp, 1f - movement * (i + 1));

            vertices[i * 8 + 7] = points[i] - binormal * (roadWidth / 2.0f + edgeWidth);
            normals[i * 8 + 7] = (-binormal - normal) / 2.0f;
            uvs[i * 8 + 7] = new Vector2(0.0f, 1f - movement * (i + 1));


            /*
            GameObject[] gameobjects = new GameObject[8];
            for (int v = 0; v < 8; v++)
            {
                gameobjects[v] = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                gameobjects[v].transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
                gameobjects[v].transform.position = vertices[i * 8 + v];
                gameobjects[v].name = (i * 8 + v).ToString();
                Debug.DrawLine(vertices[i * 8 + v], vertices[i * 8 + v] + normals[i * 8 + v], Color.magenta, 30.0f);
            }
            */
            if (i < PointsCount - 1)
            {
                for (int k = 0; k < (8 - 1); k++)
                {
                    // index = i * (8 - 1) * 3 * 2 + k * 3 * 2 + 0;
                    triangleIndices[index++] = i * 8 + k;
                    triangleIndices[index++] = (i + 1) * 8 + k;
                    triangleIndices[index++] = i * 8 + k + 1;

                    triangleIndices[index++] = i * 8 + k + 1;
                    triangleIndices[index++] = (i+1) * 8 + k;
                    triangleIndices[index++] = (i+1) * 8 + k + 1;
                }

                triangleIndices[index++] = i * 8;
                triangleIndices[index++] = i * 8 + 7;
                triangleIndices[index++] = (i + 1) * 8;

                triangleIndices[index++] = i * 8 + 7;
                triangleIndices[index++] = (i + 1) * 8 + 7;
                triangleIndices[index++] = (i + 1) * 8;

            }
        }

        Debug.Log("index count: " + index + "/" + triIndexCount);

        vertices = MixVerticesNormals(beginValue, vertices,endValue, 0);
        normals = MixVerticesNormals(beginValue, normals, endValue, 1);
        triangleIndices = MixIndices(beginEdges, triangleIndices, endEdges);
        uvs = MixUV(UVB, uvs, UBE);

        //Debug.Log("---> V:" + vertices.Length + " N:" + normals.Length+" <---");

        if (MeshFilterComponent.mesh == null)
            MeshFilterComponent.mesh = new Mesh();
        Mesh mesh = MeshFilterComponent.mesh;

        mesh.Clear();
        mesh.name = "CurveMesh";
        mesh.vertices = vertices;
        mesh.normals = normals;
        //mesh.uv = uvs;
        mesh.triangles = triangleIndices;

        MeshColliderComponent.sharedMesh = mesh;
        MeshFilterComponent.GetComponent<MeshRenderer>().material.mainTextureScale = new Vector2(2f, PointsCount / 10f * 2);

        watch.Stop();
        var elapsedMs = watch.ElapsedTicks;
        Debug.Log("Curve generated in " + watch.ElapsedMilliseconds + "ms (" + watch.ElapsedTicks + " ticks).");

        LineRenderer.positionCount = PointsCount;
        LineRenderer.SetPositions(points);

        meshPoint = points;

        ControlPointsLineRenderer.positionCount = ControlPoints.Count;
        ControlPointsLineRenderer.SetPositions(ControlPointsPositions.ToArray());
    }

    public void FixedUpdate()
    {
        Generate();
        /*
        Vector3[] v = MeshFilterComponent.mesh.vertices;
        Vector3[] n = MeshFilterComponent.mesh.normals;
        for (int i = 0; i < n.Length; i++)
        {
            Debug.DrawLine(v[i], v[i] + n[i]/4f, Color.cyan);
        }
        */
    }

float Factorial(float n)
    {
        float result = 1;
        for (int i = 0; i < n; i++)
        {
            result *= (n - i);
        }
        return result;
    }

    float Binomial_coefficient(float n, float k)
    {
        return Factorial(n) / (Factorial(k) * Factorial(n - k));
    }

    float Bernstein_polynomial(float i, float n, float u)
    {
        return Binomial_coefficient(n, i) * Mathf.Pow(u, i) * Mathf.Pow(1.0f - u, n - i);
    }

    public List<Vector3> GetCPPosition()
    {
        return ControlPointsPositions;
    }


    int[] MixIndices(int[] begin, int[] tri, int[] end)
    {
        int lb = begin.Length, lv = tri.Length, le=end.Length;
        int[] value = new int[lb + lv + le];
        for (int i = 0; i < lb; i++)
        {
            value[i] = begin[i];
        }
        for (int i = 0; i < lv; i++)
        {
            value[lb + i] = tri[i]+16;
        }
        for (int i = 0; i < le; i++)
        {
            value[lb + lv + i] = end[i];
        }
        return value;
    }

    public Vector3[] MixVerticesNormals(Vector3[,] begin, Vector3[] vertices, Vector3[,] end, int r)
    {
        int lb = begin.Length / 2, lv = vertices.Length, le = end.Length/2;
        Vector3[] value = new Vector3[lb + lv + le];

        for (int i=0; i < lb; i++)
        {
            value[i] = begin[i,r];
        }
        for (int i=0; i < lv; i++)
        {
            value[lb + i] = vertices[i];
        }
        for (int i = 0; i < le; i++)
        {
            value[lb+lv+i] = end[i, r];
        }

        return value;
    }

    public Vector2[] MixUV(Vector2[] begin, Vector2[]uv, Vector2[] end)
    {
        int lb = begin.Length, lv = uv.Length, le = end.Length;
        Vector2[] value = new Vector2[lb + lv + le];

        for (int i = 0; i < lb; i++)
        {
            value[i] = begin[i];
        }
        for (int i = 0; i < lv; i++)
        {
            value[lb + i] = uv[i];
        }
        for (int i = 0; i < le; i++)
        {
            value[lb + lv + i] = end[i];
        }

        return value;
    }

    public Vector3[,] CreateOuterPart(Vector3 point, Vector3 bi, Vector3 normal, Vector3 tang, int direction)
    {
        float edgeHeight = MeshEdgeHeight * MeshScale;
        float edgeWidth = MeshEdgeWidth * MeshScale;
        float roadWidth = MeshRoadWidth * MeshScale;

        Vector3[,] value = new Vector3[16,2];
        Vector3 binormal = bi * direction;
        Vector3 tangent = tang * direction;

        value[0,0] = point - binormal * (roadWidth / 2.0f) + tangent * (roadWidth / 2.3f);
        value[0,1] = (binormal + normal) / 2f;
        value[1,0] = point + binormal * (roadWidth / 2.0f) + tangent * (roadWidth / 2.3f);
        value[1, 1] = (-binormal + normal) / 2f;
        value[2,0] = point - binormal * (roadWidth / 4.0f) + tangent *(roadWidth / 1.5f);
        value[2,1] = (binormal + normal - tangent) / 3f;
        value[3,0] = point + binormal *(roadWidth / 4.0f) + tangent *(roadWidth / 1.5f);
        value[3,1] = (-binormal + normal - tangent) / 3f;

        value[4,0] = point - binormal *(roadWidth / 2.0f) + tangent *(roadWidth / 2.3f) + normal * edgeHeight;
        value[4,1] = (binormal + normal) / 2.0f;
        value[5,0] = point + binormal *(roadWidth / 2.0f) + tangent * (roadWidth / 2.3f) + normal * edgeHeight;
        value[5,1] = (-binormal + normal) / 2.0f;
        value[6,0] = point - binormal * (roadWidth / 4.0f) + tangent *(roadWidth / 1.5f) + normal * edgeHeight;
        value[6,1] = (binormal + normal - tangent) / 3.0f;
        value[7,0] = point + binormal *(roadWidth / 4.0f) + tangent *(roadWidth / 1.5f) + normal * edgeHeight;
        value[7,1] = (-binormal + normal - tangent) / 3.0f;

        //top surface
        value[8,0] = point - binormal * (roadWidth / 2.0f + edgeWidth) + tangent  * (roadWidth / 2.2f) + normal * edgeHeight;
        value[8,1] = (-binormal + normal + tangent) / 3f;
        value[9,0] = point + binormal *(roadWidth / 2.0f + edgeWidth) + tangent *(roadWidth / 2.2f) + normal * edgeHeight;
        value[9,1] = (binormal + normal + tangent) / 3f;
        value[10,0] = point - binormal * (roadWidth / 4.0f + edgeWidth / 2f) + tangent * (roadWidth / 1.2f) + normal * edgeHeight;
        value[10, 1] = (-binormal + normal + tangent) / 2f;
        value[11,0] = point + binormal * (roadWidth / 4.0f + edgeWidth / 2f) + tangent *(roadWidth / 1.2f) + normal * edgeHeight;
        value[11,1] =  (binormal + normal + tangent) / 2f;

        //bottom
        value[12,0] = point - binormal * (roadWidth / 2.0f + edgeWidth) + tangent * (roadWidth / 2.2f);
        value[12,1] = (-binormal - normal) / 2f;
        value[13,0] = point + binormal * (roadWidth / 2.0f + edgeWidth) + tangent * (roadWidth / 2.2f);
        value[13,1] = (binormal - normal) / 2.0f;
        value[14,0] = point - binormal * (roadWidth / 4.0f + edgeWidth / 2f) + tangent * (roadWidth / 1.2f);
        value[14,1] = (-binormal - normal + tangent) / 3f;
        value[15,0] = point + binormal * (roadWidth / 4.0f + edgeWidth / 2f) + tangent * (roadWidth / 1.2f);
        value[15,1] = (binormal - normal + tangent) / 3f;

        return value;
    }

    public int[] OuterEdgesBegin()
    {
        int[] lines = new int[]
        {
            //inner
            20,0,2,
            20,2,19,
            2,3,19,
            3,1,19,

            //inner walls
            
            20,4,0,
            21,4,20,
            0,6,2,
            4,6,0,
            2,7,3,
            6,7,2,
            3,5,1,
            7,5,3,
            1,18,19,
            5,18,1,

            //top

            21,8,4,
            22,8,21,
            4,10,6,
            8,10,4,
            6,11,7,
            10,11,6,
            7,9,5,
            11,9,7,
            5,17,18,
            9,17,5,

            //side

            22,12,8,
            23,12,22,
            8,14,10,
            12,14,8,
            10,15,11,
            14,15,10,
            11,13,9,
            15,13,11,
            9,16,17,
            13,16,9,
            //bottom
            
            16,13,15,
            16,15,14,
            23,14,12,
            23,16,14
        };
        return lines;
    }

    public int[] OuterEdgesEnd(int endValue)
    {
        endValue += 8;
        int _0 = endValue + 8,
            _1 = endValue + 9,
            _2 = endValue + 10,
            _3 = endValue + 11,
            _4 = endValue + 12,
            _5 = endValue + 13,
            _6 = endValue + 14,
            _7 = endValue + 15,
            _8 = endValue + 16,
            _9 = endValue + 17,
            _10 = endValue + 18,
            _11 = endValue + 19,
            _12 = endValue + 20,
            _13 = endValue + 21,
            _14 = endValue + 22,
            _15 = endValue + 23,
            _16 = endValue,
            _17 = endValue + 1,
            _18 = endValue + 2,
            _19 = endValue + 3,
            _20 = endValue + 4,
            _21 = endValue + 5,
            _22 = endValue + 6,
            _23 = endValue + 7;

        int[] lines = new int[]
        {
            //inner
            
            _19,_0,_2,
            _19,_2,_20,
            _2,_3,_20,
            _3,_1,_20,
            
            //inner walls
            
            _19,_4,_0,
            _18,_4,_19,
            _0,_6,_2,
            _4,_6,_0,
            _2,_7,_3,
            _6,_7,_2,
            _3,_5,_1,
            _7,_5,_3,
            _1,_21,_20,
            _5,_21,_1,
            
            //top
            
            _18,_8,_4,
            _17,_8,_18,
            _4,_10,_6,
            _8,_10,_4,
            _6,_11,_7,
            _10,_11,_6,
            _7,_9,_5,
            _11,_9,_7,
            _5,_22,_21,
            _9,_22,_5,

            //side
            
            _17,_12,_8,
            _16,_12,_17,
            _8,_14,_10,
            _12,_14,_8,
            _10,_14,_11,
            _14,_15,_11,//
            _11,_13,_9,
            _15,_13,_11,
            _9,_23,_22,
            _13,_23,_9,
            //bottom
            
            _23,_13,_15,
            _23,_15,_14,
            _16,_14,_12,
            _16,_23,_14
            
        };
        return lines;
    }

    public Vector2[] UVBegin(int pointCount)
    {
        int size = pointCount + 2;
        float movement = 1f / size;
        float xDisp = 0.5f / 9f;
        float centerDisp = 0.5f / 6f;

        float roadEnd = movement/2f;
        float yDisp = roadEnd / 3f;

        Vector2[] UV = new Vector2[]{
            new Vector2(0.5f-3*centerDisp,1f-roadEnd), //0
            new Vector2(0.5f+3*centerDisp,1f-roadEnd), //1
            new Vector2(0.5f-3*xDisp,1f-roadEnd/4f), //2
            new Vector2(0.5f+3*xDisp,1f-roadEnd/4f), //3

            new Vector2(0.5f-4*centerDisp,1f-roadEnd), //4
            new Vector2(0.5f+4*centerDisp,1f-roadEnd), //5
            new Vector2(0.5f - 4*xDisp,1f-yDisp*2), //6
            new Vector2(0.5f + 4*xDisp,1f-yDisp*2), //7

            new Vector2(0.5f-5*centerDisp,1f-roadEnd), //8
            new Vector2(0.5f+5*centerDisp,1f-roadEnd), //9
            new Vector2(0.5f - 5*xDisp,1f-yDisp), //10
            new Vector2(0.5f + 5*xDisp,1f-yDisp), //11

            new Vector2(0f,1f-roadEnd), //12
            new Vector2(1f,1f-roadEnd), //13
            new Vector2(0.5f - 6*xDisp,1f), //14
            new Vector2(0.5f + 6*xDisp,1f), //15
            };


        return UV;
    }

    public Vector2[] UVEnd(int pointCount)
    {
        int size = pointCount + 2;
        float movement = 1f / size;
        float xDisp = 0.5f / 9f;
        float yDisp = (movement / 2f) / 8f;
        float centerDisp = 0.5f / 6f;

        Vector2[] UV = new Vector2[]{
            new Vector2(0.5f+3*centerDisp,0.08f), //1
            new Vector2(0.5f-3*centerDisp,0.08f), //0 
            new Vector2(0.5f+3*xDisp,0.1f), //3
            new Vector2(0.5f-3*xDisp,0.1f), //2

            new Vector2(0.5f+4*centerDisp,0.08f), //5
            new Vector2(0.5f-4*centerDisp,0.08f), //4
            new Vector2(0.5f + 4*xDisp,0.07f), //7
            new Vector2(0.5f - 4*xDisp,0.07f), //6
 
            new Vector2(0.5f+5*centerDisp,0.08f), //9
            new Vector2(0.5f-5*centerDisp,0.08f), //8
            new Vector2(0.5f + 5*xDisp,0.03f), //11
            new Vector2(0.5f - 5*xDisp,0.03f), //10

            new Vector2(1f,0.92f), //13
            new Vector2(0f,0.92f), //12
            new Vector2(0.5f + 6*xDisp,0f), //15
            new Vector2(0.5f - 6*xDisp,0f), //14
        };


        return UV;
    }

    public void AddOuterPart(Vector3 point, Vector3 binormal, Vector3 normal, Vector3 tangent, int direction)
    {
        float edgeHeight = MeshEdgeHeight * MeshScale;
        float edgeWidth = MeshEdgeWidth * MeshScale;
        float roadWidth = MeshRoadWidth * MeshScale;

        Vector3[] vertices = new Vector3[26];
        Vector3[] normals = new Vector3[26];

        vertices[0] = point;
        normals[0] = (normal);
        vertices[1] = point - binormal * direction * (roadWidth / 2.0f);
        normals[1] = (binormal + normal) / 2.0f;
        vertices[2] = point + binormal * direction * (roadWidth / 2.0f);
        normals[2] = (-binormal + normal) / 2.0f;

        vertices[3] = point - binormal * direction * (roadWidth / 2.0f) + tangent * direction * (roadWidth / 2.3f);
        normals[3] = (binormal + normal) / 2.0f;
        vertices[4] = point + binormal * direction * (roadWidth / 2.0f) + tangent * direction * (roadWidth / 2.3f);
        normals[4] = (-binormal + normal) / 2.0f;
        vertices[5] = point - binormal * direction * (roadWidth / 4.0f) + tangent * direction * (roadWidth / 1.5f);
        normals[5] = (binormal + normal) / 2.0f;
        vertices[6] = point + binormal * direction * (roadWidth / 4.0f) + tangent * direction * (roadWidth / 1.5f);
        normals[6] = (-binormal + normal) / 2.0f;

        vertices[7] = point - binormal * direction * (roadWidth / 2.0f) + normal * edgeHeight;
        normals[7] = (binormal + normal) / 2.0f;
        vertices[8] = point + binormal * direction * (roadWidth / 2.0f) + normal * edgeHeight;
        normals[8] = (-binormal + normal) / 2.0f;
        vertices[9] = point - binormal * direction * (roadWidth / 2.0f) + tangent * direction * (roadWidth / 2.3f) + normal * edgeHeight;
        normals[9] = (binormal + normal) / 2.0f;
        vertices[10] = point + binormal * direction * (roadWidth / 2.0f) + tangent * direction * (roadWidth / 2.3f) + normal * edgeHeight;
        normals[10] = (-binormal + normal) / 2.0f;
        vertices[11] = point - binormal * direction * (roadWidth / 4.0f) + tangent * direction * (roadWidth / 1.5f) + normal * edgeHeight;
        normals[11] = (binormal + normal) / 2.0f;
        vertices[12] = point + binormal * direction * (roadWidth / 4.0f) + tangent * direction * (roadWidth / 1.5f) + normal * edgeHeight;
        normals[12] = (-binormal + normal) / 2.0f;

        //top surface
        vertices[13] = point - binormal * direction * (roadWidth / 2.0f + edgeWidth) + normal * edgeHeight;
        normals[13] = (-binormal + normal);
        vertices[14] = point + binormal * direction * (roadWidth / 2.0f + edgeWidth) + normal * edgeHeight;
        normals[14] = (binormal + normal);
        vertices[15] = point - binormal * direction * (roadWidth / 2.0f + edgeWidth) + tangent * direction * (roadWidth / 2.2f) + normal * edgeHeight;
        normals[15] = (-binormal + normal);
        vertices[16] = point + binormal * direction * (roadWidth / 2.0f + edgeWidth) + tangent * direction * (roadWidth / 2.2f) + normal * edgeHeight;
        normals[16] = (binormal + normal);
        vertices[17] = point - binormal * direction * (roadWidth / 4.0f + edgeWidth / 2f) + tangent * direction * (roadWidth / 1.2f) + normal * edgeHeight;
        normals[17] = (-binormal + normal);
        vertices[18] = point + binormal * direction * (roadWidth / 4.0f + edgeWidth /2f) + tangent * direction * (roadWidth / 1.2f) + normal * edgeHeight;
        normals[18] = (binormal + normal);

        //bottom
        vertices[19] = point - binormal * direction * (roadWidth / 2.0f + edgeWidth);
        normals[19] = (-binormal - normal);
        vertices[20] = point + binormal * direction * (roadWidth / 2.0f + edgeWidth);
        normals[20] = (binormal - normal);
        vertices[21] = point - binormal * direction * (roadWidth / 2.0f + edgeWidth) + tangent * direction * (roadWidth / 2.2f);
        normals[21] = (-binormal - normal);
        vertices[22] = point + binormal * direction * (roadWidth / 2.0f + edgeWidth) + tangent * direction * (roadWidth / 2.2f);
        normals[22] = (binormal - normal);
        vertices[23] = point - binormal * direction * (roadWidth / 4.0f + edgeWidth / 2f) + tangent * direction * (roadWidth / 1.2f);
        normals[23] = (-binormal - normal);
        vertices[24] = point + binormal * direction * (roadWidth / 4.0f + edgeWidth / 2f) + tangent * direction * (roadWidth / 1.2f);
        normals[24] = (binormal - normal);

        vertices[25] = point;
        normals[25] = (-normal);

        //vertices[1] = point + binormal * (roadWidth / 2.0f + edgeWidth) + normal * (edgeHeight);
        //normals[1] = (binormal + normal) / 2.0f;

        int[] lines = new int[]
        {
            0,1,3,
            0,4,2,
            0,3,5,
            0,6,4,
            0,5,6,
            1,9,3,
            7,9,1,
            4,8,2,
            10,8,4,
            3,11,5,
            9,11,3,
            6,10,4,
            12,10,6,
            5,12,6,
            11,12,5,
            //top
            7,13,9,
            13,15,9,
            9,17,11,
            15,17,9,
            11,18,12,
            17,18,11,
            10,12,16,
            16,12,18,
            8,10,14,
            14,10,16,
            //side
            19,21,13,
            13,21,15,
            21,23,15,
            15,23,17,
            23,24,17,
            17,24,18,
            24,22,18,
            18,22,16,
            22,20,16,
            16,20,14,
            //bottom
            25,21,19,
            25,23,21,
            25,24,23,
            25,22,24,
            25,20,22
            

        };

        if (direction == 1)
        {
            if (Begin == null)
            {
                Begin = new GameObject();
                MeshRenderer r = Begin.AddComponent<MeshRenderer>();
                Begin.AddComponent<MeshFilter>();
                Begin.transform.parent = this.transform;
                MeshFilter msh = Begin.GetComponent<MeshFilter>();
                msh.mesh = new Mesh();
                msh.mesh.vertices = vertices;
                msh.mesh.SetIndices(lines, MeshTopology.Triangles, 0);
                msh.mesh.normals = normals;
                msh.mesh.RecalculateBounds();
                r.allowOcclusionWhenDynamic = false;
                Begin.GetComponent<Renderer>().material = mat;
            }
            else
            {
                Begin.GetComponent<MeshFilter>().mesh.vertices = vertices;
            }

        }
        else
        {
            if (End == null)
            {
                End = new GameObject();
                MeshRenderer r = End.AddComponent<MeshRenderer>();
                End.AddComponent<MeshFilter>();
                End.transform.parent = this.transform;
                MeshFilter msh = End.GetComponent<MeshFilter>();
                msh.mesh = new Mesh();
                msh.mesh.vertices = vertices;
                msh.mesh.SetIndices(lines, MeshTopology.Triangles, 0);
                msh.mesh.normals = normals;
                msh.mesh.RecalculateBounds();
                r.allowOcclusionWhenDynamic = false;
                End.GetComponent<Renderer>().material = mat;
            }
            else
            {
                End.GetComponent<MeshFilter>().mesh.vertices = vertices;
            }
        }
            
    }


}
