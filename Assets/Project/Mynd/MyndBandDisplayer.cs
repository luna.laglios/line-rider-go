﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyndBandDisplayer : MonoBehaviour {
    private DisplayData data;
    public Transform arrowAttention;
    public Text attention;
    public Transform arrowMeditation;
    public Text meditation;
    public Transform arrowZone;
    public Text zone;

    // Use this for initialization
    void Start () {
        data = FindObjectOfType<DisplayData>();
	}
	
	// Update is called once per frame
	void Update () {
        var att = data.Attention;
        var med = data.Meditation;

        arrowAttention.localEulerAngles = new Vector3(arrowAttention.localEulerAngles.x, arrowAttention.localEulerAngles.y, Mathf.Lerp(90, -90, (float)att / 100));
        attention.text = att.ToString();

        arrowMeditation.localEulerAngles = new Vector3(arrowMeditation.localEulerAngles.x, arrowMeditation.localEulerAngles.y, Mathf.Lerp(90, -90, (float)data.Meditation / 100));
        meditation.text = med.ToString();

        float av = (float)(data.Attention + data.Meditation) / 2;
        arrowZone.localEulerAngles = new Vector3(arrowZone.localEulerAngles.x, arrowZone.localEulerAngles.y, Mathf.Lerp(90, -90, av / 100));
        zone.text = av.ToString();
    }
}
