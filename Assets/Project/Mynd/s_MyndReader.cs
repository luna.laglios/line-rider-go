﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class s_MyndReader : MonoBehaviour {
    public Text mainText;
    private bool valueRead;
    public Color textHigh;

	// Use this for initialization
	void Start () {
        valueRead = false;
	}
	
	// Update is called once per frame
	void Update () {
        
        if (valueRead)
        {
            mainText.text="<color=#" + ColorUtility.ToHtmlStringRGB(textHigh) + ">" + "<b><size=20>Data</size></b></color>";
            mainText.text += "___";
        }
        else
        {
            mainText.text = "<color=#" + ColorUtility.ToHtmlStringRGB(textHigh) + ">" + "<b><size=20>VOiD</size></b></color>";
            mainText.alignment = TextAnchor.UpperCenter;
            mainText.text += "\nI lived as I dreamt,\nAnd the seasons pass as I went." +
                "\nThe crows caw the loss of winter,\nAs flowers bloomed and a rose a new singer," +
                "\nI remember the salty breeze of summer,\nAnd the sea of fantasy." +
                "\nThe sun radiating with rays of honey,\nTruth be told, the future couldn't be clearer";
        }
	}

}
