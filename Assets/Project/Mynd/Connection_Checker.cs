﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Connection_Checker : MonoBehaviour {
    public DisplayData data;
    public Text t;
    private int step;
    private float time;
    private Coroutine c;

    // Use this for initialization
    private void Awake()
    {
        t.text = LR_GLobal.text["MyndChecker0"];
    }

    void Start () {
        step = 0;
        time = 0;
	}
	
	// Update is called once per frame
	void Update () {
	    if(data.PoorSignal == 0 && c == null)
        {
            c = StartCoroutine("ClosePopUp");
        }
        else
        {
            time += Time.deltaTime;
            if (time > 1f)
            {
                string value = LR_GLobal.text["MyndChecker0"];
                for (int i = 0; i < step; i++)
                {
                    value += ".";
                }
                t.text = value;
                time -= 1f;
                step = step + 1 > 5 ? 0 : step+1;
            }
        }
	}

    IEnumerator ClosePopUp()
    {
        float prog = 0;
        var s = this.transform.localScale.x;
        while(prog < 1f)
        {
            prog += 2f * Time.deltaTime;
            var r = Mathf.Lerp(s, 0, prog);
            this.transform.localScale = new Vector3(r, transform.localScale.y, r);
            yield return null;
        }
        this.gameObject.SetActive(false);
    }
}
