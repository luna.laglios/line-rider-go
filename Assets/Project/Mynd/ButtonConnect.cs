﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonConnect : OGButton {
    //Coroutine co;
    Transform Parent;

    public new void Start()
    {
        Parent = this.transform.parent;
    }

    public override void MainAction()
    {
        base.MainAction();
        Parent.gameObject.SetActive(false);
    }

    /*
    IEnumerator Connect()
    {
        UnityThinkGear.StopStream();
        UnityThinkGear.Close();
        yield return new WaitForSeconds(1.5f);
        UnityThinkGear.Init(true);
        yield return new WaitForSeconds(1.5f);
        UnityThinkGear.StartStream();
    }
    */
}
