﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public struct SpeedData
{
    public float speedValue;
    public string display;
    public SpeedData(float v, string d)
    {
        speedValue = v;
        display = d;
    }
};

public static class LR_GLobal {

	public static bool isPasswordOn;
    public static int masterVolume;
    public static int musicVolume;
    public static int sfxVolume;
    public static int speed;
    public static SpeedData[] speedData = new SpeedData[] {
        new SpeedData(0.5f, "TrackSpeed0"), //very slow
        new SpeedData(1f, "TrackSpeed1"), //Slow
        new SpeedData(1.5f, "TrackSpeed2"), //Normal
        new SpeedData(2f, "TrackSpeed3"), //Quick
        new SpeedData(3f, "TrackSpeed4"), //Very Quick
        new SpeedData(5f, "TrackSpeed5") //Quickest
    };
    public static bool playerInvertedControl;
    public static bool playerInTrain;

    //language variable
    public static int language = 1;
    public const int FR = 0,
        ENG = 1;
    public static Dictionary<string, string> text;

    const string fileName = "/config.txt";

    static LR_GLobal()
    {
        string path = Application.persistentDataPath + fileName;
        if (!File.Exists(path))
        {
            Debug.Log("Creating File");
            var y =File.Create(path);
            y.Close();
            Init();
            SaveData();
        }
        else
        {
            LoadData();
        }
        LoadText();
    }

    private static void Init()
    {
        isPasswordOn = false;
        masterVolume = 6;
        musicVolume = 6;
        sfxVolume = 6;
        speed = 1;
        playerInvertedControl = false;
        language = ENG;
    }

    #region Save / Load
    public static void LoadData()
    {
        string path = Application.persistentDataPath + fileName;
        StreamReader sr = new StreamReader(path);
        
        //read passwordOn
        string tmp = sr.ReadLine();
        if (tmp == null) return;
        isPasswordOn = tmp[0].Equals('0') ? false : true;
       
        //read volume parameters
        tmp = sr.ReadLine();
        if (tmp == null) return;
        masterVolume = int.Parse(tmp);
        tmp = sr.ReadLine();
        if (tmp == null) return;
        musicVolume = int.Parse(tmp);
        tmp = sr.ReadLine();
        if (tmp == null) return;
        sfxVolume = int.Parse(tmp);
        tmp = sr.ReadLine();
        if (tmp == null) return;
        speed = int.Parse(tmp);
        tmp = sr.ReadLine();
        if (tmp == null) return;
        playerInvertedControl = tmp[0].Equals('0') ? false : true;
        //language
        tmp = sr.ReadLine();
        if (tmp == null) return;
        language = int.Parse(tmp);

        sr.Close();
    }

    public static void SaveData()
    {
        string path = Application.persistentDataPath + fileName;
        StreamWriter sw = new StreamWriter(path);
        //save passwordOn
        sw.WriteLine(isPasswordOn ? "1" : "0");
        sw.WriteLine(masterVolume.ToString());
        sw.WriteLine(musicVolume.ToString());
        sw.WriteLine(sfxVolume.ToString());
        sw.WriteLine(speed.ToString());
        sw.WriteLine(playerInvertedControl ? "1" : "0");
        sw.WriteLine(language);
        sw.Close();
    }
    #endregion

    public static void switchPasswordMode()
    {
        isPasswordOn = !isPasswordOn;
        SaveData();
    }

    public static void LoadText()
    {
        string path = "", jsonstring= "";
        path = Application.streamingAssetsPath + "/lang/" +
        (language == ENG ? "eng" : "fr") +
        ".json";
        //only way of reading from streaming assets in android
        WWW reader = new WWW(path);
        while (!reader.isDone) { }
        if (LR_GLobal.language == FR)
            jsonstring = Encoding.UTF7.GetString(reader.bytes, 0, reader.bytes.Length);
        else
            jsonstring = reader.text;
        reader.Dispose();

        string[] words = jsonstring.Split('"');
        bool continu = true; int pos = 1;


        text = new Dictionary<string, string>();

        while (continu)
        {
            //Debug.Log(words[pos] + " : " + words[pos + 2]);
            text.Add(words[pos], words[pos + 2]);
            if (!words[pos + 3][0].Equals(','))
            {
                continu = false;
            }
            else
            {
                pos += 4;
            }
        }
    }
}
