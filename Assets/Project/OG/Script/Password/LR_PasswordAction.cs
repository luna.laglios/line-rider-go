﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LR_PasswordAction : OGButton {
    public bool save;
    public TMP_Text text;

    private bool saveButton;
    new public void Start()
    {
        text = transform.GetChild(0).GetComponent<TMP_Text>();
        if (save && SceneManager.GetActiveScene().name.Equals("MainMenu"))
        {
            text.text = "Save";
            saveButton = true;
        }
        else if(save)
        {
            text.text = LR_GLobal.text["PasswordPanel3"];
        }
    }

    public override void MainAction()
    {
        if(!save)
            LR_PasswordProtection.ResetInput();
        else
        {
            if (saveButton)
            {
                LR_PasswordProtection.Save();
            }
            else
            {
                LR_PasswordProtection.CheckPassword();
            }
        }
    }
}
