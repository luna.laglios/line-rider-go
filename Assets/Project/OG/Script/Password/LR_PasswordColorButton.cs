﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LR_PasswordColorButton : OGButton {
    public string color;
    public Color originalColor;
    public Image bg;

    new private void Start()
    {
        base.Start();
        originalColor = this.gameObject.transform.GetChild(0).GetComponent<Image>().color;
        bg = this.gameObject.transform.GetChild(0).GetComponent<Image>();
    }

    private void OnEnable()
    {
        
    }

    // Update is called once per frame
    public override void MainAction()
    {
        StopAllCoroutines();
        StartCoroutine("Pressed");
        LR_PasswordProtection.addChar(color);
    }

    IEnumerator Pressed()
    {
        Debug.Log("Coroutine has started and bg=" + bg == null ? "Null" : "Asigned");
        Color newC = originalColor + new Color(0.75f, 0.75f, 0.75f);
        bg.color = newC;
        float i = 0;
        while (i < 1f)
        {
            i += Time.deltaTime * 2;
            bg.color = Color.Lerp(newC,originalColor,i);
            yield return null;
        }
    }
}
