﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LR_PasswordProtection : MonoBehaviour {
    //private bool passwordOn;
    [SerializeField]
    private string pass;
    public string tmpPass;

    public static LR_PasswordProtection instance;
    public TMP_Text TopMessage,
        BottomDisplayer;

    private const string C_R = "R",
        C_G = "G",
        C_B = "B",
        C_Y = "Y",
        pname = "/p.txt";
    private bool onMainMenu;

	// Use this for initialization
	public void Start () {
		if(instance == null)
        {
            instance = new LR_PasswordProtection();
            instance.tmpPass = "";
            string p_path = Application.persistentDataPath + pname;
            if (File.Exists(p_path))
            {
                StreamReader sr = new StreamReader(p_path);
                instance.pass = sr.ReadLine();
                sr.Close();
            }
            else
            {
                File.Create(p_path);
                instance.pass = "";
            }
        }
        onMainMenu = SceneManager.GetActiveScene().name.Equals("MainMenu") ? true : false;
        //BottomDisplayer.text = "";

    }

    private void Update()
    {
        if(onMainMenu)
            BottomDisplayer.text = LR_GLobal.text["PasswordPanel2-1"] + instance.pass + " \n"+ LR_GLobal.text["PasswordPanel2-2"] + instance.tmpPass;
        else
        {
            BottomDisplayer.text = LR_GLobal.text["PasswordPanel1"];
            for(int i = 0; i < instance.tmpPass.Length; i++)
            {
                BottomDisplayer.text += "*" +
                    "";
            }
            BottomDisplayer.text += "_";
        }
    }

    #region Public Functions
    public static void CheckPassword()
    {
        if (instance.pass.Equals(instance.tmpPass)){
            if(!LRLevelManager.editor)
            {
                LRLevelManager.editor = !LRLevelManager.editor;
            }
            else
            {
                SceneManager.LoadScene("MainMenu");
            }
        } else
        {
            instance.tmpPass = "";
        }
    }

    public static bool CheckPasswordRet()
    {
        return instance.pass.Equals(instance.tmpPass);
    }

    public static void addChar(string b)
    {
        if (instance.tmpPass.Length < 6)
        {
            instance.tmpPass += b;
            //Debug.Log("Adding char to password : " + instance.tmpPass);
        }
    }

    public static void ResetInput()
    {
        instance.tmpPass = "";
    }

    public static void Save()
    {
        string path = Application.persistentDataPath + pname;
        if (!File.Exists(path))
        {
            Debug.Log("Creating File");
            File.Create(path);
        }
        StreamWriter sw = new StreamWriter(path);
        sw.Write(instance.tmpPass);
        instance.pass = instance.tmpPass;
        sw.Close();
        instance.tmpPass = "";
    }
    #endregion
}
