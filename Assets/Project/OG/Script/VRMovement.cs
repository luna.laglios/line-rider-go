﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class VRMovement : MonoBehaviour {

    //notes
    /*
     * OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad, OVRInput.Controller.RTrackedRemote) : Get touchpad TOUCH POSITION
     * OVRInput.Get(OVRInput.Button.PrimaryTouchpad) : Get touchpad click input
     */
    // Use this for initialization

    [Header("Movement Parameters")]
    public bool canMove=true;
    public Transform direction;
    [Range(0.5f,2f)]
    public float speed=1f;
    [Range(0.1f, 0.9f)]
    public float threshold=0.2f;
    private bool horizontal;

    [Header("Rotation Parameters")]
    public bool canTurn=true;
    public Transform ControllerR;
    public Transform ControllerL;
    private Transform Controller;
    [Range(25f, 180f)]
    public float angleThreshold;
    [Range(0.5f, 2f)]
    public float angleSpeed;

    [Header("Pickable Option(s)")]
    public bool canPick=true;
    private bool itemInHand;
    private float itemDistance;
    private GameObject pickedObject;

    [Header("Other Necessities")]
    public GameObject originR;
    public GameObject originL;
    private GameObject origin;
    private LineRenderer rayRenderer;
    public GameObject rayExtremityR;
    public GameObject rayExtremityL;
    private GameObject rayExtremity;
    private OVRInput.Controller c;
    private OVRPlugin.Handedness useHand;
    public Transform orbPointer;
    private Vector3 orbCloseScale;
    public Transform Neck;

    public GameObject iconHorizontal;
    public GameObject iconVertical;

    [Header("Can Return To Another Screen?")]
    public bool canUseBack = false;
    [Tooltip("If null, return to title screen")]
    public string goBack;

    [Header("Debug Dipslay")]
    public bool displayDebug;
    public Text debugPosTex;
    public Text debugRotation;
    public Text debugObject;


    //password Menu
    private GameObject passwordMenuPrefabs;
    public GameObject passwordMenu;

    void Start () {
        LR_GLobal.playerInTrain = false;
        pickedObject = null;
        itemInHand = false;
        setHand();
        horizontal = true;
        passwordMenuPrefabs = Resources.Load("Prefabs/PasswordMenu", typeof(GameObject)) as GameObject;
        orbCloseScale = orbPointer.localScale;
        orbPointer.gameObject.SetActive(false);
        if(iconHorizontal != null)
            iconHorizontal.SetActive(false);
        if(iconVertical != null)
            iconVertical.SetActive(false);
    }

    void setHand()
    {
        if(displayDebug)
            debugPosTex.text = OVRPlugin.GetDominantHand() == OVRPlugin.Handedness.RightHanded ? " Right " : OVRPlugin.GetDominantHand() == OVRPlugin.Handedness.LeftHanded ? " Left " : "Nothing";
        if ((useHand = OVRPlugin.GetDominantHand())== OVRPlugin.Handedness.RightHanded)
        {
            c = OVRInput.Controller.RTrackedRemote;
            Controller = ControllerR;
            origin = originR;
            rayExtremity = rayExtremityR;
        }
        else
        {
            c = OVRInput.Controller.LTrackedRemote;
            Controller = ControllerL;
            origin = originL;
            rayExtremity = rayExtremityL;
        }
        rayRenderer = origin.GetComponent<LineRenderer>();
    }


	
	// Update is called once per frame
	void Update () {
        if(useHand != OVRPlugin.GetDominantHand())
        {
            setHand();
        }

        movementAction();

        turnAction();

        if (canPick)
        {
            triggerAction();
        }
        else if(orbPointer.gameObject.activeSelf)
        {
            orbPointer.gameObject.SetActive(false);
        }


        if (itemInHand)
        {
            if (displayDebug)
            {
                debugObject.text = pickedObject.name;
                if (pickedObject.GetComponent<Collider>())
                {
                    debugObject.text += " w\\ colliderer";
                }
                if (pickedObject.GetComponent<LRCP>())
                {
                    debugObject.text += "\nIs an LRCP";
                }else if (pickedObject.GetComponent<LREraser>())
                {
                    debugObject.text += "\nIs an Eraser";
                }else if (pickedObject.GetComponent<OGControlPoint>())
                {
                    debugObject.text += "\nIs a ball painter";
                }else
                    debugObject.text += "\nIs a generic pickable object";
            }
        } else
        {
            if(displayDebug)
                debugObject.text = "Empty Nothing";
        }
        padAction();
        //set the visual help
        if (!LR_GLobal.playerInTrain)
        {
            if (iconHorizontal == null || iconVertical == null)
                return;

            if (!iconHorizontal.activeSelf && horizontal)
            {
                iconHorizontal.SetActive(true);
                iconVertical.SetActive(false);
            }
            else if (!iconVertical.activeSelf && !horizontal)
            {
                iconHorizontal.SetActive(false);
                iconVertical.SetActive(true);
            }
        }else if(iconHorizontal.activeSelf || iconVertical.activeSelf)
        {
            iconVertical.SetActive(false);
            iconHorizontal.SetActive(false);
        }
    }

    void movementAction()
    {
        Vector2 inputP = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad, c);
        Vector3 movement = new Vector3(0f, 0f, 0f);
        if (canMove && !itemInHand) //movePlayer
        {
            if (inputP.x > threshold || inputP.x < -threshold)
            {
                movement.x = inputP.x;
            }
            if (inputP.y > threshold || inputP.y < -threshold)
            {
                if(horizontal)
                    movement.z = inputP.y;
                else
                    movement.y = inputP.y;
            }
            if (movement.magnitude > 1f)
            {
                movement = movement.normalized;
            }
            var f = direction.forward; f.y = 0f; f = f.normalized;
            var r = direction.right; r.y = 0f; r = r.normalized;
            var u = Vector3.up;
            transform.position += (f * movement.z * (LR_GLobal.playerInvertedControl ? -1 : 1) + r * movement.x + u * movement.y) * speed * Time.deltaTime;
        }
        else if(itemInHand) //moveObject
        {
            if (inputP.y > 0.5f || inputP.y < -0.5f)
            {
                itemDistance += inputP.y*Time.deltaTime * (LR_GLobal.playerInvertedControl ? -1 : 1);
                itemDistance = itemDistance < 0.1f ? 0.1f : itemDistance > 10f ? 10f : itemDistance;
                pickedObject.transform.localPosition = new Vector3(0f, 0f, itemDistance*1.3f);
            }
        }
        if (displayDebug)
        {
            debugPosTex.text = "Movement : " + movement.ToString();
        }
    }

    void triggerAction()
    {
        bool trigger = OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger,c);
        bool triggerDown = OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger,c);
        bool triggerUp = OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger,c);
        if (itemInHand && pickedObject == null)
        {
            itemInHand = false;
        }

        if(displayDebug)
            debugRotation.text = "Trigger : " + trigger + "\nTriggerDown  : " + triggerDown + "\nTriggerUp : " + triggerUp;
        RaycastHit hit;


        if (trigger)
        {
            rayRenderer.SetPosition(0, origin.transform.position);
            rayRenderer.enabled = true;
            //if hit something
            if (Physics.Raycast(origin.transform.position, origin.transform.forward, out hit, 40f))
            {
                if (itemInHand)
                    rayRenderer.SetPosition(1, pickedObject.transform.position);
                else
                {
                    rayRenderer.SetPosition(1, hit.point);
                }

                //gestion interraction----------------------
                if (triggerDown && !itemInHand)
                {
                    OGPickable target = hit.collider.GetComponent<OGPickable>();
                    if (target != null)
                    {
                        itemDistance = hit.distance>20f? 20f : hit.distance<0.1f? 0.1f : hit.distance;
                        target.SetPick(true);
                        itemInHand = true;
                        pickedObject = target.gameObject;
                        pickedObject.transform.SetParent(Controller);
                        pickedObject.transform.localPosition = new Vector3(0f, 0f, itemDistance);
                        //pickedObject.transform.localRotation = new Quaternion(0f, 0f, 0f, 1f);

                    }
                }
            } //if nothing is hit
            else if (itemInHand)
            {
                rayRenderer.SetPosition(1, pickedObject.transform.position);
            }
            else
                rayRenderer.SetPosition(1, rayExtremity.transform.position);


        }
        else
        {
            if (triggerUp)
                if (itemInHand)
                {
                    pickedObject.transform.SetParent(null);
                    pickedObject.GetComponent<OGPickable>().SetPick(false);
                    itemInHand = false;
                    pickedObject = null;
                }
                else
                {
                    if (Physics.Raycast(origin.transform.position, origin.transform.forward, out hit, 40f))
                    {
                        OGButton target1 = hit.collider.GetComponent<OGButton>();
                        if (target1 != null)
                            target1.SendMessage("MainAction");
                    }

                }
            rayRenderer.enabled = false;
        }

        if (!itemInHand && Physics.Raycast(origin.transform.position, origin.transform.forward, out hit, 40f))
        {
            orbPointer.gameObject.SetActive(true);
            orbPointer.transform.position = hit.point;
            orbPointer.localScale = orbCloseScale * (hit.distance + 1f) / 2f;
        }
        else
        {
            orbPointer.gameObject.SetActive(false);
        }
    }

    void turnAction()
    {
        if (canTurn)
        {
            Quaternion rot = OVRInput.GetLocalControllerRotation(c);
            Transform angleC = Controller;
            angleC.rotation.eulerAngles.Set(0f, angleC.eulerAngles.y, 0f);
            GameObject tmp = new GameObject();
            tmp.transform.eulerAngles = new Vector3(0f, rot.eulerAngles.y, 0f); ;
            Vector3 angleC2 = tmp.transform.forward;

            float angle = Vector3.SignedAngle(this.transform.forward, angleC2, Vector3.up);
            if(displayDebug)
                debugRotation.text = "Rotation : " + rot.eulerAngles + " Angle : " + angle + " Obtained from : " + this.transform.forward + " " + angleC2;
            if (Mathf.Abs(angle) > angleThreshold)
            {
                if (angle < 0)
                    this.transform.Rotate(0f, -10f * angleSpeed * Time.deltaTime, 0f);
                else
                    this.transform.Rotate(0f, 10f * angleSpeed * Time.deltaTime, 0f);
            }
        }
    }

    void padAction()
    {
        bool clickDown = OVRInput.GetDown(OVRInput.Button.One,c);
        bool click = OVRInput.Get(OVRInput.Button.One,c);
        if (click && itemInHand)
        {
            pickedObject.SendMessage("MainActionContinuous");
        }
        if (clickDown)
        {
            if (itemInHand)
                pickedObject.SendMessage("MainActionOnce");
            else if(canMove)
            {
                horizontal = !horizontal;
            }
        }

        backButton();
    }

    void backButton()
    {
        bool back = OVRInput.GetUp(OVRInput.Button.Back, c);
        if (back && canUseBack)
        {
            if ( (LR_GLobal.isPasswordOn && SceneManager.GetActiveScene().name == "Editor") || SceneManager.GetActiveScene().name == "Playground")
            {
                if (passwordMenu == null)
                {
                    passwordMenu = Instantiate(passwordMenuPrefabs);
                    passwordMenu.transform.parent = Neck;
                    passwordMenu.transform.localPosition = new Vector3(0, 0, 1.5f);
                    passwordMenu.transform.localEulerAngles = Vector3.zero;
                    passwordMenu.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                }
                else
                {
                    Destroy(passwordMenu.gameObject);
                    passwordMenu = null;
                }
            }
            else
            {
                if (!goBack.Equals(""))
                {
                    SceneManager.LoadScene(goBack);
                }
                else
                {
                    SceneManager.LoadScene("MainMenu");
                }
            }
        }
    }
}