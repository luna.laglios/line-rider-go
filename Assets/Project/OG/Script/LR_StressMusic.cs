﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LR_StressMusic : MonoBehaviour {
    private Vector3 calm;
    private Vector3 dynamic;
    private DisplayData data;
    private Coroutine _corout;
    private bool isCalm;
    private bool previousState;

    [SerializeField]
    private float _stressLevel;

    public bool Debug_Change;
    public bool _debug;

	// Use this for initialization
	void Start () {
        calm = new Vector3(0, 0, 2);
        dynamic = new Vector3(0, 0, -2);
        this.transform.localPosition = calm;
        data = FindObjectOfType<DisplayData>();
        isCalm = true;
        _stressLevel = 0;
        previousState = LR_GLobal.playerInTrain;
    }
	
	// Update is called once per frame
	void Update () {
#if !UNITY_EDITOR
            stress_management();
    #else
        if (Debug_Change)
        {
            Debug_Change = false;
            if(_corout != null)
                    StopAllCoroutines();
            if (isCalm)
            {
                _corout = StartCoroutine("ChangeMusic", dynamic);
            }else
                _corout = StartCoroutine("ChangeMusic", calm);
            isCalm = !isCalm;
        }
    #endif
    }

    public float _stressSpeed_UpMeditation=4f, _stressSpeed_UpAttention=1f,
        _stressSpeed_Down=1f,
        _stressLowerLimit = 25, _stressUpperLimit = 75,
        _attention_LowerThreshold = 25, _attention_UpperThreshold = 75,
        _meditation_LowerThreshold = 20, _meditation_UpperThreshold = 63;

    public TMPro.TMP_Text stressText;
    private void stress_management()
    {
        if (SceneManager.GetActiveScene().name == "Editor" || SceneManager.GetActiveScene().name == "Playground")
        {
            if (_debug)
                debug_stress();
            else
            {

                //calculate the stress level
                //--Stress change for MEDITATION
                if (data.Meditation != 0)
                {
                    if (data.Meditation < _meditation_LowerThreshold)
                    {
                        _stressLevel += _stressSpeed_UpMeditation * Time.deltaTime;
                        _stressLevel = _stressLevel > 100 ? 100 : _stressLevel;
                    }
                    else if (data.Meditation > _meditation_UpperThreshold)
                    {
                        _stressLevel -= _stressSpeed_Down * Time.deltaTime;
                        _stressLevel = _stressLevel < 0 ? 0 : _stressLevel;
                    }
                }


                //--Stress change for ATTENTION
                if (data.Attention != 0)
                {
                    if (data.Attention < _attention_LowerThreshold)
                    {
                        _stressLevel += _stressSpeed_UpAttention * Time.deltaTime;
                        _stressLevel = _stressLevel > 100 ? 100 : _stressLevel;
                    }
                    else if (data.Attention > _attention_UpperThreshold)
                    {
                        _stressLevel -= _stressSpeed_Down * 0.25f * Time.deltaTime;
                        _stressLevel = _stressLevel < 0 ? 0 : _stressLevel;
                    }
                }

                //natural decay -- kinda slow
                _stressLevel -= _stressSpeed_Down * 0.1f * Time.deltaTime;
                _stressLevel = _stressLevel < 0 ? 0 : _stressLevel;

                //display update
                if (stressText != null)
                    stressText.text = (_stressLevel < _stressLowerLimit ? "<color=#0000ffff>" : _stressLevel > _stressUpperLimit ? "<color=#ff0000ff>" : "<color=#777777ff>" ) + ((int)Mathf.Floor(_stressLevel)).ToString() + "</color>";

                //now we need to process the stress effects --> adaptative music
                musicManagment();

            }//end if not in debug test
        }// end if scene is right
    }

    void musicManagment()
    {
        //when a change if state occurs
        if (LR_GLobal.playerInTrain != previousState)
        {

            previousState = LR_GLobal.playerInTrain;
            if (LR_GLobal.playerInTrain)
            {
                if (_corout != null)
                    StopAllCoroutines();
                _corout = StartCoroutine(ChangeMusic(dynamic, 10));
            }
            else
            {
                if (_corout != null)
                    StopAllCoroutines();
                _corout = StartCoroutine(ChangeMusic(calm, 10));
            }
        }
        //if player is in the train
        else if (LR_GLobal.playerInTrain)
        {
            if (!isCalm && _stressLevel < _stressLowerLimit)
            {
                if (_corout != null)
                    StopAllCoroutines();
                _corout = StartCoroutine(ChangeMusic(dynamic, 1));
                isCalm = true;
            }
            else if (isCalm && _stressLevel > _stressUpperLimit)
            {
                if (_corout != null)
                    StopAllCoroutines();
                _corout = StartCoroutine(ChangeMusic(calm, 1));
                isCalm = false;
            }
        }
    }
  
    
    private void debug_stress()
    {
        if (!isCalm && data.Attention < 25)
        {
            if (_corout != null)
                StopAllCoroutines();
            _corout = StartCoroutine(ChangeMusic(calm, 1));
            isCalm = true;
        }
        else if (isCalm && data.Attention > 75)
        {
            if (_corout != null)
                StopAllCoroutines();
            _corout = StartCoroutine(ChangeMusic(dynamic, 1));
            isCalm = false;
        }
    }

    [Range(0, 1)]
    public float change_speed = 0.005f;
    IEnumerator ChangeMusic(Vector3 Change, int speed)
    {
        float prog = 0;
        while (prog < 1)
        {
            prog += change_speed * Time.deltaTime * speed;
            transform.localPosition = Vector3.Lerp(transform.localPosition,Change,prog);
            yield return null;
        }
        _corout = null;
    }


    public float GetStress()
    {
        return _stressLevel;
    }

    public void setCalm(bool v)
    {
        isCalm = v;
    }
}
