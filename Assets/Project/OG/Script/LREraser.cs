﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LREraser : OGPickable {
    public CurveMesh Bezier;
    public Text debugCollision;
    private Vector3 originalPos;
	// Use this for initialization
	void Start () {
        originalPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        if (picked)
        {
            transform.localEulerAngles = new Vector3(0f, -90f, 0f);
            transform.localScale = new Vector3(3f, 3f, 3f);
        }
	}
}
