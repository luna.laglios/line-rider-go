﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OGResetEditor : OGButton {
    public Transform painter;
    public CurveMesh bezier;
    private Vector3 pPos;

	// Use this for initialization
	new void Start () {
        pPos = painter.position;
	}
	
	// Update is called once per frame
	new void Update () {
		
	}

    public override void MainAction()
    {
        base.MainAction();
        painter.position = pPos;
        for (int i = 0; i < bezier.ControlPoints.Count; i++)
        {
            Destroy(bezier.ControlPoints[i].gameObject);
        }
        bezier.ControlPoints.Clear();
    }
}
