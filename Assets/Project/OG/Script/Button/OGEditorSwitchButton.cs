﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OGEditorSwitchButton : OGButton {

    public override void MainAction()
    {
        LRLevelManager.editor = !LRLevelManager.editor;
    }
}
