﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum langChoice
{
    FR,
    ENG
}

public class LR_LanguageButton : OGButton {
    public langChoice language;
    public Text debug;

    public override void MainAction()
    {
        base.MainAction();
        debug.text = "Nothing";
        try
        {
            switch (language)
            {
                case (langChoice.FR):
                    LR_GLobal.language = LR_GLobal.FR;
                    LR_GLobal.LoadText();
                    break;
                case (langChoice.ENG):
                    LR_GLobal.language = LR_GLobal.ENG;
                    LR_GLobal.LoadText();
                    break;
            }
            LR_GLobal.SaveData();
        }catch(System.Exception e)
        {
            debug.text = e.ToString();
        }
    }
}
