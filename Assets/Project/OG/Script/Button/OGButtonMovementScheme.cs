﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OGButtonMovementScheme : OGButton {
    public TMPro.TMP_Text display;

    public new void Start()
    {
        display.text = LR_GLobal.playerInvertedControl ? LR_GLobal.text["MovementDisplay2"] : LR_GLobal.text["MovementDisplay1"];
    }

    public override void MainAction()
    {
        LR_GLobal.playerInvertedControl = !LR_GLobal.playerInvertedControl;
        display.text = LR_GLobal.playerInvertedControl ? LR_GLobal.text["MovementDisplay2"] : LR_GLobal.text["MovementDisplay1"];
        LR_GLobal.SaveData();
    }
}
