﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OGButtonConfirmation : OGButton {
    public OGTrackManager brain;
    public int value;

    public override void MainAction()
    {
        brain.SendMessage("Confirm", value);
    }
}
