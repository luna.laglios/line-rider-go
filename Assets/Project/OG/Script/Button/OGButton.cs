﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OGButton : MonoBehaviour {
    protected bool isPushed;
    public bool debugPushing;
	// Use this for initialization
	public void Start () {
        isPushed = false;
        debugPushing = false;
	}

    private void OnEnable()
    {
        Image bg = this.gameObject.GetComponent<Image>();
        if(bg != null)
        {
            if (bg.color != Color.white)
                bg.color = Color.white;
        }
    }

    // Update is called once per frame
    virtual public void Update () {
        if (debugPushing)
        {
            MainAction();
            debugPushing = false;
            Debug.Log(this.name + " has been pushed");
        }
	}

    virtual public void MainAction()
    {
        if (!isPushed)
        {
            isPushed = true;
            StartCoroutine("Blink");
        }
    }

    protected IEnumerator Blink()
    {
        Image bg = this.gameObject.GetComponent<Image>();
        if(bg != null)
            bg.color = Color.black;
        yield return new WaitForSeconds(1f);
        if (bg != null)
            bg.color = Color.white;
        yield return new WaitForSeconds(0.05f);
        isPushed = false;
    }
}
