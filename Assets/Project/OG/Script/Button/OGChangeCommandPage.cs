﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OGChangeCommandPage : OGButton {
    public int pageNumber;

    public override void MainAction()
    {
        base.MainAction();
        LR_CommandDisplayerAnimation.page = pageNumber;
    }
}
