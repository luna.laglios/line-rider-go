﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OGSpeedDisplayer : MonoBehaviour {
    public TMPro.TMP_Text displayer;
	
	// Update is called once per frame
	void Update () {
        var s = LR_GLobal.speed;
        displayer.text = LR_GLobal.text[LR_GLobal.speedData[LR_GLobal.speed].display];
	}
}
