﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OGSpeedModifier : OGButton {
    public bool increase;

    public override void MainAction()
    {
        base.MainAction();
        if (!increase)
        {
            LR_GLobal.speed--;
            LR_GLobal.speed = LR_GLobal.speed > 0 ? LR_GLobal.speed : 0;
        }
        else
        {
            LR_GLobal.speed++;
            LR_GLobal.speed = LR_GLobal.speed > LR_GLobal.speedData.Length - 1 ? LR_GLobal.speedData.Length - 1 : LR_GLobal.speed;
        }
        Debug.Log("Size : " + LR_GLobal.speedData.Length + " Speed val : " + LR_GLobal.speed);
        LR_GLobal.SaveData();
    }
}
