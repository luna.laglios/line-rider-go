﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OGButtonSend : OGButton {

    public enum Manager // your custom enumeration
    {
        TrackManager,
        MainMenuManager,
        SoundManger
    };
    public enum sendValue
    {
        Nothing,
        Number,
        String,
        GameObject,
    }

    //variables
    public GameObject brain;
    public Manager classType = Manager.TrackManager;
    [Header("Fonction to call")]
    public string fonction;
    public sendValue whatToSend = sendValue.Nothing;
    [Header("Parameter")]
    public float number;
    public string message;
    public GameObject gameObj;
    private Component comp;
    [Space(1f)]
    [Header("Protection")]
    public bool passwordLock;

    public new void Start()
    {
        var rec = GetComponent<RectTransform>();
        if (rec == null)
            Destroy(this.gameObject);
        else
        {
            var col = gameObject.AddComponent<BoxCollider>();
            col.size = new Vector3(rec.sizeDelta.x+rec.sizeDelta.x/15f, rec.sizeDelta.y + rec.sizeDelta.y / 15f, 1f);
        }
        switch (classType)
        {
            case Manager.TrackManager:
                comp = brain.GetComponent<OGTrackManager>();
                break;
            case Manager.MainMenuManager:
                comp = brain.GetComponent<LRMainMenuManager>();
                break;
            case Manager.SoundManger:
                comp = brain.GetComponent<LRMusicController>();
                break;
            default:
                comp = null;
                break;
        }

    }

    public override void MainAction()
    {
        if (!LR_GLobal.isPasswordOn || !passwordLock) {
            base.MainAction();
            send();
        }
    }


    void send()
    {
        if (comp == null)
            return;
        switch (whatToSend)
        {
            case sendValue.Nothing:
                comp.SendMessage(fonction);
                break;
            case sendValue.Number:
                comp.SendMessage(fonction, number);
                break;
            case sendValue.String:
                comp.SendMessage(fonction, message);
                break;
            case sendValue.GameObject:
                comp.SendMessage(fonction, gameObj);
                break;
            default:
                return;
        }

    }
}
