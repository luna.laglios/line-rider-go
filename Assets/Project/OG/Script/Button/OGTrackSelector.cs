﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OGTrackSelector : OGButton {
    public OGTrackManager list;
    public int position;
	// Use this for initialization
	new void Start () {
		
	}
	
	// Update is called once per frame
	new void Update () {
		
	}

    public override void MainAction()
    {
        base.MainAction();
        list.SendMessage("newSelection", this.transform);
    }
}
