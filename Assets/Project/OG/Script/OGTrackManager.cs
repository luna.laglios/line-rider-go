﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class OGTrackManager : MonoBehaviour {

    public Transform selector;
    public Transform buttons;
    public CurveMesh bezier;
    public Text errorText;
    private int nbButton;
    private int selectionPos;
    private int listPosition;
    private List<FileInfo> listFile;
    public Transform player;
    public Canvas panel;
    private bool confirmationAsked;
    private int confirm;
    public Text debugConfirm;
    

	// Use this for initialization
	void Start () {
        nbButton = buttons.childCount;
        listPosition = 0;
        listFile = new List<FileInfo>();
        updateList();
        confirmationAsked = false;
        confirm = 0;
        selector.gameObject.SetActive(true);
        errorText.gameObject.SetActive(false);
        panel.gameObject.SetActive(false);
    }
    // Update is called once per frame
    void Update()
    {
        //debugConfirm.text = "Asked? : " + confirmationAsked +
        //    "\nconfirmValue : " + confirm;
        if (!confirmationAsked)
        {
            //debugConfirm.text += "\nNo orders";
        }
        if (!confirmationAsked && confirm != 0)
        {
            confirmationAsked = false;
            confirm = 0;
        }
    }

    #region Interraction

    void newSelection(Transform t)
    {
        if (confirmationAsked)
            return;

        selector.gameObject.SetActive(true);
        selector.transform.position = t.position;
        selectionPos = t.GetComponent<OGTrackSelector>().position;
    }

    void moveList(int v)
    {
        if (confirmationAsked)
            return;

        listPosition += v;
        listPosition = listPosition < listFile.Count? (listPosition >= 0 ? listPosition : 0) : listFile.Count-1;
        updateList();
;   }

    void getList()
    {
        if (!Directory.Exists(Application.persistentDataPath + "/Tracks"))
        {
            Directory.CreateDirectory(Application.persistentDataPath + "/Tracks");
        }
        var info = new DirectoryInfo(Application.persistentDataPath + "/Tracks/");
        var list = info.GetFiles();
        if (list.Length > 0) {
            listFile.Clear();
            List<FileInfo> s1 = new List<FileInfo>();
            List<FileInfo> s2 = new List<FileInfo>();
            s1.Add(list[0]);
            for (int i = 1; i < list.Length; i++)
            {
                if (list[i].ToString().Length == s1[0].ToString().Length)
                {
                    s1.Add(list[i]);
                }
                else
                {
                    s2.Add(list[i]);
                }
            }
            for(int i = 0; i < s1.Count; i++)
            {
                listFile.Add(s1[i]);
            }
            for (int i = 0; i < s2.Count; i++)
            {
                listFile.Add(s2[i]);
            }
        }
    }

    public void updateList()
    {
        //debugConfirm.text = "getting list";
        getList();
        if (listPosition < listFile.Count)
        {
            //debugConfirm.text += "\nGoing into the 1st loop"; 
            int nb = 0;
            for(int i=listPosition;i<listFile.Count && nb < buttons.childCount; i++)
            {
                //debugConfirm.text += "\nElement " + i + " " + nb + " L" + listFile[i].ToString().Length;
                buttons.GetChild(nb).GetComponent<Text>().text = listFile[i].ToString();
                    nb++;
            }
            //debugConfirm.text += "\nShinished listing";
            if (nb < nbButton)
            {
                //debugConfirm.text += "\nFilling holes";
                for (int i = nb; i < nbButton; i++)
                {
                    buttons.GetChild(nb).GetComponent<Text>().text = "";
                    nb++;
                }
            }
        }
    }

    #endregion


    #region Manipulation
    private void delete()
    {
        if (confirmationAsked)
            return;
        askConfirm();
        StartCoroutine("deleteConfirm");
    }

    IEnumerator deleteConfirm()
    {
        //debugConfirm.text = "\nDelete Waiting";
        while (confirm == 0)
        {
            yield return null;
        }

        if (confirm == 1 && listFile.Count > 1)
        {

            //debugConfirm.text += "\nConfirmation received";
            int p = listPosition + selectionPos;
            if (!(p < listFile.Count))
            {
                delinkPanel();
                yield break;
            }
            if (listPosition == listFile.Count - 1)
                listPosition--;
            string name = listFile[p].ToString();
            string location = Application.persistentDataPath + "/Tracks/" + name;
            File.Delete(location);
            //debugConfirm.text += "\nFile Deleted\n Updating list";
            //debugConfirm.text += "\nList deleted";
        }
        //debugConfirm.text += "\nHidding panel";
        delinkPanel();
        updateList();
        //debugConfirm.text += "\nPanel Hidden";
    }

    void saveTrack()
    {
        if (confirmationAsked)
            return;
        askConfirm();
        StartCoroutine("saveTrackConfirmed");
    }

    IEnumerator saveTrackConfirmed()
    {
        //debugConfirm.text = "\nSave Waiting";
        while (confirm == 0)
        {
            yield return null;
        }

        if (confirm == 1)
        {
            //debugConfirm.text += "\nConfirmation Received";
            Debug.Log("saving");
            string m_Path = Application.persistentDataPath;
            string name = "/Tracks/Track";
            int nb = 0;
            string tmp = m_Path + name + nb.ToString() + ".txt";
            bool test = false;
            while (!test)
            {
                if (!File.Exists(tmp))
                {
                    test = true;
                }
                else
                {
                    nb++;
                    tmp = m_Path + name + nb.ToString() + ".txt";
                }
            }
            //debugConfirm.text += "\nSaving File";
            Debug.Log("Save File at " + tmp);
            var file = File.Open(tmp, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            var writer = new StreamWriter(file);
            writer.WriteLine(bezier.ControlPoints.Count.ToString() + "\r\n");
            for (int i = 0; i < bezier.ControlPoints.Count; i++)
            {
                writer.WriteLine(bezier.ControlPoints[i].position.x + "\r\n");
                writer.WriteLine(bezier.ControlPoints[i].position.y + "\r\n");
                writer.WriteLine(bezier.ControlPoints[i].position.z + "\r\n");
            }
            //debugConfirm.text += "\nClosing Streams";
            writer.Close();
            file.Close();
            //debugConfirm.text += "\nUpdating list";
            updateList();
            //debugConfirm.text += "\nFinished";
        }
        //debugConfirm.text += "\nHidding Panels";
        delinkPanel();
    }

    void loadTrack()
    {
        if (confirmationAsked)
            return;
        else if (listPosition + selectionPos < listFile.Count)
        {
            askConfirm();
            StartCoroutine("loadTrackConfirmed");
        }
    }


    IEnumerator loadTrackConfirmed()
    {
        while (confirm == 0)
            yield return null;

        if (confirm == 1)
        {
            int p = listPosition + selectionPos;
            if (!(p < listFile.Count))
            {
                delinkPanel();
                yield break;
            }
            string name = listFile[p].ToString();
            string location = Application.persistentDataPath + "/Tracks/" + name;
            if (File.Exists(location))
            {
                var file = File.Open(location, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                var reader = new StreamReader(file);
                string tmp;
                tmp = reader.ReadLine();
                int nbcpt;
                if (System.Int32.TryParse(tmp, out nbcpt))
                {
                    List<Vector3> newPos = new List<Vector3>();
                    float x, y, z;
                    for (int i = 0; i < nbcpt; i++)
                    {
                        tmp = reader.ReadLine(); tmp = reader.ReadLine();
                        x = float.Parse(tmp);
                        tmp = reader.ReadLine(); tmp = reader.ReadLine();
                        y = float.Parse(tmp);
                        tmp = reader.ReadLine(); tmp = reader.ReadLine();
                        z = float.Parse(tmp);
                        Vector3 v = new Vector3(x, y, z);
                        newPos.Add(v);
                    }
                    if (newPos.Count > 0)
                    {
                        for (int i = 0; i < bezier.ControlPoints.Count; i++)
                        {
                            Destroy(bezier.ControlPoints[i].gameObject);
                        }
                        bezier.ControlPoints.Clear();

                        for (int i = 0; i < nbcpt; i++)
                        {
                            GameObject cp = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                            cp.transform.position = newPos[i];
                            cp.transform.rotation = Quaternion.identity;
                            LRCP options = cp.gameObject.AddComponent<LRCP>();
                            options.usesGravity = false;
                            options.erasable = true;
                            options.Bezier = bezier;
                            cp.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
                            Collider c = cp.GetComponent<Collider>();
                            if (c != null)
                            {
                                c.isTrigger = true;
                                (c as SphereCollider).radius = 1f;
                            }
                            bezier.ControlPoints.Add(cp.transform);
                        }
                    }
                    else
                    {
                        errorText.gameObject.SetActive(true);
                        errorText.text = "No points created";
                    }
                }
                else
                {
                    errorText.gameObject.SetActive(true);
                    errorText.text = "Couldn't get number of points";
                }
                reader.Close(); file.Close();
            }
            else
            {
                errorText.gameObject.SetActive(true);
                errorText.text = "File does not exist anymore";
                //errorText.text = location;
            }
        }
        delinkPanel();
    }

    void delinkPanel()
    {
        confirmationAsked = false;
        confirm = 0;
        panel.gameObject.SetActive(false);
    }

    void askConfirm()
    {
        confirmationAsked = true;
        panel.gameObject.SetActive(true);
        //panel.transform.parent = player;
        //panel.transform.localPosition = new Vector3(0f, 0f, 1f);
    }

    void Confirm(int value)
    {
        confirm = value;
    }
    #endregion
}
