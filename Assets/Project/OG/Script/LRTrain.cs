﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FMODUnity;

public class LRTrain : MonoBehaviour {
    public CurveMesh bezier;
    public Transform lookDirection;
    public Text countdown;
    public LRLevelManager editor;
    public Transform player;
    public bool running;
    private int nextStop;
    private float speed;

    private float distanceMin=0.2f;

    public bool debug=false;
    public Text debugSpeed;
    private GameObject sphere;
    private LineRenderer line;

    private float maxSpeed = 2f;

    private FMODUnity.StudioEventEmitter emitter;

    private LR_StressMusic stressManager;
    private GameObject passwordMenuPrefabs;
    private GameObject passwordMenu;

    // Use this for initialization
    void Start () {
        running = false;
        nextStop = 2;
        countdown.text = "";
        emitter = GetComponent<FMODUnity.StudioEventEmitter>();
        stressManager = FindObjectOfType<LR_StressMusic>();
        passwordMenuPrefabs = Resources.Load("Prefabs/PasswordMenu", typeof(GameObject)) as GameObject;
        StartCoroutine("StartUp");
        //ratio = 0.5f - ((float)bezier.ControlPoints.Count / (float)bezier.meshPoint.Length);
        //Debug.Log("Ratio : " + ratio + " - " + bezier.ControlPoints.Count + " " + bezier.meshPoint.Length);
        if (debug)
        {
            sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphere.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            debugSpeed.gameObject.SetActive(true);
            line = this.gameObject.AddComponent<LineRenderer>();
            line.SetWidth(0.045f, 0.045f);
        }
	}

    private IEnumerator StartUp()
    {
        yield return new WaitForSeconds(1f);
        countdown.text = "3";
        yield return new WaitForSeconds(1f);
        countdown.text = "2";
        yield return new WaitForSeconds(1f);
        countdown.text = "1";
        yield return new WaitForSeconds(1f);
        countdown.text = "!";
        yield return new WaitForSeconds(1f);
        countdown.text = "";
        running = true;
    }

    private IEnumerator End()
    {
        Debug.Log("The End has started");
        yield return new WaitForSeconds(2f);
        player.transform.parent = null;
        LRLevelManager.editor = true ;
        Destroy(this.gameObject);
    }

    void Update()
    {
        if (!LRLevelManager.editor && player.transform.parent == null)
            Destroy(this.gameObject);

        if (running)
        {
            Vector3 p = bezier.meshPoint[nextStop];
            float dist = Vector3.Distance(transform.position, p);
            lookDirection.rotation = Quaternion.LookRotation(p - transform.position);
            transform.position = Vector3.MoveTowards(transform.position, p, Time.deltaTime * getSpeed());
            transform.rotation = Quaternion.Slerp(transform.rotation,lookDirection.rotation,Time.deltaTime * ( getSpeed()/6f));

            if (dist <= distanceMin)
            {
                nextStop++;
            }

            if (nextStop >= bezier.meshPoint.Length)
            {
                StartCoroutine("End");
                running = false;
            }

        }

        if (OVRInput.GetUp(OVRInput.Button.Back))
        {
            if (!LR_GLobal.isPasswordOn)
                StartCoroutine("End");
            else
            {
                if(passwordMenu == null)
                {
                    passwordMenu = Instantiate(passwordMenuPrefabs);
                    passwordMenu.transform.parent = this.transform;
                    passwordMenu.transform.localPosition = new Vector3(0, 1f, 1.5f);
                    passwordMenu.transform.localEulerAngles = Vector3.zero;
                    passwordMenu.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                    var o = player.GetComponent<VRMovement>();
                    o.canPick = true;
                }
                else
                {
                    Destroy(passwordMenu);
                    passwordMenu = null;
                    var o = player.GetComponent<VRMovement>();
                    o.canPick = false;
                }
            }
        }
    }

    private float getSpeed()
    {
        Vector2 inputP = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad, OVRPlugin.GetDominantHand()==OVRPlugin.Handedness.RightHanded ? OVRInput.Controller.RTrackedRemote : OVRInput.Controller.LTrackedRemote);
        Vector3 tmp = this.transform.eulerAngles;
        transform.eulerAngles = new Vector3(0f, lookDirection.eulerAngles.y, lookDirection.eulerAngles.z);
        Vector3 reference = transform.forward;
        transform.eulerAngles = tmp;

        /*float angle = Vector3.SignedAngle(reference, lookDirection.forward,Vector3.forward);
        //Debug.Log(angle.ToString());
        if(angle < 0f)
        {
            speed += (Mathf.Abs(angle)/180f) * Time.deltaTime;
        }
        else
        {
            speed -= (angle/180f) * Time.deltaTime;
        }
        */

        float angle = Vector3.Angle(reference, lookDirection.forward);
        float up, down;
        up = Vector3.Angle(lookDirection.forward, Vector3.up);
        down = Vector3.Angle(lookDirection.forward, Vector3.down);
        if (down < up)
        {
            speed += (Mathf.Abs(angle) / 360f) * Time.deltaTime;
        }
        else
        {
            speed -= (angle / 360f)/2f * Time.deltaTime;
        }


        var GlobalSpeed = LR_GLobal.speedData[LR_GLobal.speed].speedValue;
        var maximumSpeed = maxSpeed + GlobalSpeed;
        speed = speed < 0f ? 0f : speed > maximumSpeed ? maximumSpeed : speed;
        
        if (debug)
        {
            debugSpeed.text ="Angle : "+ angle + "\nSpeed : " + speed;
            Debug.Log("Speed = " + speed);
            Debug.DrawLine(lookDirection.position, lookDirection.position + lookDirection.forward * 4f, Color.red);
            Debug.DrawLine(lookDirection.position, lookDirection.position + reference * 4f, Color.blue);
            line.SetPosition(0, this.transform.position);
            line.SetPosition(1, lookDirection.position + lookDirection.forward * 4f);
        }

        float finalSpeed = (GlobalSpeed + speed + (inputP.y) / 1.25f) * stressMultiplier();

        float speedRatio = finalSpeed / maximumSpeed;
        emitter.SetParameter("WindStrenght", speedRatio);

        var n = bezier.meshPoint.Length - 1;
        float distAct = Vector3.Distance(this.transform.position, bezier.meshPoint[n]);
        int seuil = Mathf.FloorToInt(n * 0.90f);
        //need to slow down at the end
        if (nextStop < seuil)
        {
            return finalSpeed;
        }
        else
        {
            var distTotal = Vector3.Distance(bezier.meshPoint[seuil],bezier.meshPoint[n]);
            return Mathf.Lerp(finalSpeed, 0f, 1f - (distAct / distTotal) );
        }
        
    }

    float stressMultiplier()
    {
        return Mathf.Lerp(1.25f, 0.75f, 1 - (stressManager.GetStress()) / 100);
    }
}
