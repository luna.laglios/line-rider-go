﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OGControlPoint : OGPickable {
    public CurveMesh bezier;
    public GameObject toolShed;
    [Range(-2f, 2f)]
    public float minimum;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (!picked && transform.position.y < minimum)
        {
            transform.position = new Vector3(transform.position.x, minimum, transform.position.z);
        }

        if (!picked && this.transform.parent == null)
        {
            this.transform.parent = toolShed.transform;
        }
	}

    override public void MainActionOnce()
    {
        if (transform.position.y > minimum)
        {
            GameObject cp = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            cp.transform.position = this.transform.position;
            cp.transform.rotation = Quaternion.identity;
            LRCP options = cp.gameObject.AddComponent<LRCP>();
            options.usesGravity = false;
            options.erasable = true;
            options.Bezier = bezier;
            cp.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
            Collider c = cp.GetComponent<Collider>();
            if (c != null)
            {
                c.isTrigger = true;
                (c as SphereCollider).radius = 1.25f;
            }

            bezier.ControlPoints.Add(cp.transform);
        }
    }
}
