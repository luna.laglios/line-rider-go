﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalizedText : MonoBehaviour {
    public string keyValue;
    private TMPro.TMP_Text highText;
    private UnityEngine.UI.Text lowText;

	// Use this for initialization
	void Start () {
        lowText = GetComponent<UnityEngine.UI.Text>();
        highText = GetComponent<TMPro.TMP_Text>();
    }
	
	// Update is called once per frame
	void Update () {
        var t = LR_GLobal.text[keyValue];
        if (lowText != null)
            lowText.text = t;
        if (highText != null)
            highText.text = t;
    }
}
