﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LRMusicController : MonoBehaviour {

    FMOD.Studio.Bus music;
    FMOD.Studio.Bus sfx;
    FMOD.Studio.Bus master;
    private int masterVolume;
    public Transform masterBar;
    private int musicVolume;
    public Transform musicBar;
    private int sfxVolume;
    public Transform sfxBar;
    private bool change;
    private int range;

	// Use this for initialization
	void Start () {
        sfx = FMODUnity.RuntimeManager.GetBus("bus:/Master/SoundEffect");
        music = FMODUnity.RuntimeManager.GetBus("bus:/Master/Music");
        master = FMODUnity.RuntimeManager.GetBus("bus:/Master");
        change = true;
        range = masterBar.childCount;
        masterVolume = LR_GLobal.masterVolume;
        musicVolume = LR_GLobal.musicVolume;
        sfxVolume = LR_GLobal.sfxVolume;
    }
	
	// Update is called once per frame
	void Update () {
        if (change)
        {
            master.setVolume((float)masterVolume/(float)range);
            displayMasterBar();
            music.setVolume((float)musicVolume/(float)range);
            displayMusicBar();
            sfx.setVolume((float)sfxVolume/ (float)range);
            displaySFXBar();
            change = false;
            LR_GLobal.masterVolume = masterVolume;
            LR_GLobal.musicVolume = musicVolume;
            LR_GLobal.sfxVolume = sfxVolume;
            LR_GLobal.SaveData();

            Debug.Log("new master volume : " + (float)masterVolume / (float)range);
        }
	}

    void setMaster(float v)
    {
        change = true;
        if (v > 0)
        {
            masterVolume = masterVolume+1<range? masterVolume+1 : range;
        }
        else if (v < 0)
        {
            masterVolume = masterVolume-1>0? masterVolume-1 : 0;
        }
    }

    void displayMasterBar()
    {
        if (masterBar != null)
        {
            for (int i = 0; i < masterVolume; i++)
            {
                masterBar.GetChild(i).GetComponent<Image>().color = Color.white;
            }
            for (int i = masterVolume; i < range; i++)
            {
                masterBar.GetChild(i).GetComponent<Image>().color = Color.black;
            }
        }
    }

    void setMusic(float v)
    {
        change = true;
        if (v > 0)
        {
            musicVolume = musicVolume + 1 < range ? musicVolume + 1 : range;
        }
        else if (v < 0)
        {
            musicVolume = musicVolume - 1 > 0 ? musicVolume - 1 : 0;
        }
    }

    void displayMusicBar()
    {
        if (musicBar != null)
        {
            for (int i = 0; i < musicVolume; i++)
            {
                musicBar.GetChild(i).GetComponent<Image>().color = Color.white;
            }
            for (int i = musicVolume; i < range; i++)
            {
                musicBar.GetChild(i).GetComponent<Image>().color = Color.black;
            }
        }
    }

    void setSFX(float v)
    {
        change = true;
        if (v > 0)
        {
            sfxVolume = sfxVolume + 1 < range ? sfxVolume + 1 : range;
        }
        else if (v < 0)
        {
            sfxVolume = sfxVolume - 1 > 0 ? sfxVolume - 1 : 0;
        }
    }


    void displaySFXBar()
    {
        if (sfxBar != null)
        {
            for (int i = 0; i < sfxVolume; i++)
            {
                sfxBar.GetChild(i).GetComponent<Image>().color = Color.white;
            }
            for (int i = sfxVolume; i < range; i++)
            {
                sfxBar.GetChild(i).GetComponent<Image>().color = Color.black;
            }
        }
    }


}
