﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NameDisplay : MonoBehaviour {
    private Transform pa;
    private GameObject text;
    public string displayName;
	// Use this for initialization
	void Start () {
        pa = transform.parent;
        text = new GameObject();
        text.transform.parent = this.gameObject.transform;
        text.transform.localEulerAngles = new Vector3(0f,180f,0f);
        text.transform.localPosition = Vector3.zero;
        text.transform.localScale = pa.localScale;
        TextMesh t = text.AddComponent<TextMesh>();
        t.text = displayName;
        t.alignment = TextAlignment.Center;
        t.anchor = TextAnchor.UpperCenter;
        t.fontSize = 20;
	}
	
	// Update is called once per frame
	void Update () {
        transform.LookAt(Camera.main.transform);
        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, transform.localEulerAngles.z);
        transform.position = new Vector3(pa.position.x, pa.position.y + transform.lossyScale.y, pa.position.z);
    }
}
