﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TesseractInnerMovement : MonoBehaviour {
    private Vector2 off;
    private Material mat;
	// Use this for initialization
	void Start () {
        off = Vector2.zero ;
        mat = GetComponent<Renderer>().material;
	}
	
	// Update is called once per frame
	void Update () {
        off += new Vector2(0.0f,0.1f) * Time.deltaTime;
        if (off.y > 1f)
            off -= new Vector2(0f, 1f);
        mat.SetTextureOffset("_MainTex",off);

    }
}
