﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(MeshFilter))]
//[RequireComponent(typeof(MeshRenderer))]
//[ExecuteInEditMode]
public class Tesseract : MonoBehaviour {
    public Material Glass;
    public Material Meta;
    public Material Wire;

    protected Mesh _mesh;
    protected Mesh _wire;

    private Vector3[] baseCube;
    private Vector3[] cube;
    private MeshFilter metaCube;
    private MeshFilter glassCube;
    private MeshFilter wireCube;
    private int[] indices;
    private int[] lines;

    private float advancement;
	// Use this for initialization
	void Start () {
        _mesh = new Mesh();
        cube = new Vector3[12];
        DefineBase();
        DefineIndice();
        //DefineLines();
        //_mesh.SetIndices(lines, MeshTopology.Lines, 0);
        InitCube();
        _mesh.vertices = cube;
        _mesh.SetIndices(indices,MeshTopology.Quads,0);
        //_mesh.RecalculateNormals();
        advancement = 0f;
        GameObject metaC = new GameObject();
        metaC.AddComponent<MeshRenderer>();
        metaCube = metaC.AddComponent<MeshFilter>();
        GameObject glassC = new GameObject();
        glassC.AddComponent<MeshRenderer>();
        glassCube = glassC.AddComponent<MeshFilter>();

        metaCube.mesh = _mesh; glassCube.mesh = _mesh;
        metaCube.transform.name = "Meta Cube"; glassCube.transform.name = "Glass Cube";
        metaCube.transform.parent = this.transform; glassCube.transform.parent = this.transform;
        metaCube.transform.localPosition=Vector3.zero; glassCube.transform.localPosition=Vector3.zero;
        metaCube.transform.localScale=new Vector3(1,1,1); glassCube.transform.localScale=new Vector3(1,1,1);
        metaCube.GetComponent<Renderer>().material = Meta;
        glassCube.GetComponent<Renderer>().material = Glass;

        DefineLines();
        _wire = new Mesh();
        _wire.vertices = cube;
        _wire.SetIndices(lines, MeshTopology.Lines, 0);
        GameObject wireC = new GameObject();
        wireC.AddComponent<MeshRenderer>();
        wireCube = wireC.AddComponent<MeshFilter>();
        wireCube.transform.parent = this.transform;
        wireCube.mesh = _wire;
        wireCube.transform.localPosition = Vector3.zero;
        wireCube.transform.localScale = new Vector3(1, 1, 1);
        wireCube.transform.GetComponent<Renderer>().material = Wire;
        wireCube.transform.name = "Wire Cube";
    }

    protected void OnDestroy()
    {
        if (_mesh != null)
        {
            if (Application.isPlaying) Destroy(_mesh);
            else DestroyImmediate(_mesh);
        }
        _mesh = null;
    }

    // Update is called once per frame
    void Update () {
        ModifyCube();
        _mesh.vertices = cube;
        _wire.vertices = cube;
	}

    void DefineBase()
    {
        baseCube  = new Vector3[16] {
            //outer cube
            new Vector3 (-1, -1, -1),
            new Vector3 (1, -1, -1),
            new Vector3 (1, 1, -1),
            new Vector3 (-1, 1, -1),
            new Vector3 (-1, 1, 1),
            new Vector3 (1, 1, 1),
            new Vector3 (1, -1, 1),
            new Vector3 (-1, -1, 1),
            //inner cube
            new Vector3 (-0.5f, -0.5f, -0.5f),
            new Vector3 (0.5f, -0.5f, -0.5f),
            new Vector3 (0.5f, 0.5f, -0.5f),
            new Vector3 (-0.5f, 0.5f, -0.5f),
            new Vector3 (-0.5f, 0.5f, 0.5f),
            new Vector3 (0.5f, 0.5f, 0.5f),
            new Vector3 (0.5f, -0.5f, 0.5f),
            new Vector3 (-0.5f, -0.5f, 0.5f),
        };
    }

    void InitCube()
    {
        cube = new Vector3[16];
        for(int i=0; i < 16; i++)
        {
            cube[i] = baseCube[i];
        }
    }

    void DefineIndice()
    {
        indices = new int[96] {
        //outer cube
        3,2,1,0,//front face ext
        7,6,5,4,//back face ext 
        3,4,5,2,//top face ext
        0,1,6,7,//bottom face ext
        4,3,0,7,//left face ext 
        2,5,6,1,//right ext
        //inner cube
        11,10,9,8,//front face int
        15,14,13,12,//back face int 
        11,12,13,10,//top face int
        8,9,14,15,//bottom face int
        12,11,8,15,//left face int 
        10,13,14,9,//right int
        //top ext-int faces
        11,3,2,10,
        12,4,3,11,
        12,4,5,13,
        10,2,5,13,
        //bottom ext-int faces
        8,9,1,0,
        15,8,0,7,
        6,7,15,14,
        9,14,6,1,
        //side left ext-int faces
        11,3,0,8,
        12,15,7,4,
        //side right ext-int faces
        13,5,6,14,
        10,9,1,2
        };
        
    }

    void ModifyCube()
    {
        if (advancement > 1)
        {
            advancement = 0.5f*Time.deltaTime;
        }
        
        cube[0] = Vector3.Lerp(baseCube[0], baseCube[7], advancement);
        cube[1] = Vector3.Lerp(baseCube[1], baseCube[6], advancement);
        cube[2] = Vector3.Lerp(baseCube[2], baseCube[5], advancement);
        cube[3] = Vector3.Lerp(baseCube[3], baseCube[4], advancement);
        cube[4] = Vector3.Lerp(baseCube[4], baseCube[12], advancement);
        cube[5] = Vector3.Lerp(baseCube[5], baseCube[13], advancement);
        cube[6] = Vector3.Lerp(baseCube[6], baseCube[14], advancement);
        cube[7] = Vector3.Lerp(baseCube[7], baseCube[15], advancement);
        
        cube[8] = Vector3.Slerp(baseCube[8], baseCube[0], advancement);
        cube[9] = Vector3.Slerp(baseCube[9], baseCube[1], advancement);
        cube[10] = Vector3.Slerp(baseCube[10], baseCube[2], advancement);
        cube[11] = Vector3.Slerp(baseCube[11], baseCube[3], advancement);
        
        cube[12] = Vector3.Slerp(baseCube[12], baseCube[11], advancement);
        cube[13] = Vector3.Slerp(baseCube[13], baseCube[10], advancement);
        cube[14] = Vector3.Slerp(baseCube[14], baseCube[9], advancement);
        cube[15] = Vector3.Slerp(baseCube[15], baseCube[8], advancement);

        advancement += 0.5f * Time.deltaTime;
    }

   void DefineLines()
    {
        lines = new int[]
        {
            0,1,
            1,2,
            2,3,
            3,0,
            4,5,
            5,6,
            6,7,
            7,4,
            0,7,
            1,6,
            2,5,
            3,4,

            8,9,
            9,10,
            10,11,
            11,8,
            12,13,
            13,14,
            14,15,
            15,12,
            8,15,
            9,14,
            10,13,
            11,12,

            11,3,
            12,4,
            15,7,
            8,0,
            13,5,
            14,6,
            9,1,
            10,2
        };
    }
}
