﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationConstant : MonoBehaviour {
    public int direction;
    [SerializeField]
    private bool rotate;
    [SerializeField]
    private float timePass, timeRotation, timeStill;
    private float rotationAngle = 360f/12f * 2;
    private Quaternion oldRotation, newRotation;

	// Use this for initialization
	void Start () {
        timePass = 0;
        timeRotation = 0.5f;
        timeStill = 1f;
        rotate = false;
	}

    // Update is called once per frame
    void FixedUpdate()
    {
        timePass += Time.fixedDeltaTime;
        if (!rotate)
        {
            if(timePass > timeStill)
            {
                rotate = true;
                timePass = 0;
                oldRotation = transform.localRotation;
                newRotation = oldRotation * Quaternion.Euler(new Vector3(direction * rotationAngle, 0, 0));
            }

        }
        else
        {
            if (timePass < timeRotation)
            {
                transform.localRotation = Quaternion.Lerp(oldRotation, newRotation, timePass);
            }
            else
            {
                transform.localRotation = Quaternion.Lerp(oldRotation, newRotation, timePass);
                rotate = false;
                timePass = 0;
            }
        }
    }
}
