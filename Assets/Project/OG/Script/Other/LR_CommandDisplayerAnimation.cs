﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LR_CommandDisplayerAnimation : MonoBehaviour {

    [Header("Page 1")]
    public GameObject Page1;
    public Transform arrowClick,
        hand, hand2;
    private Vector3 arrowOriPos,
        handOriPos, handOriPos2;
    private Coroutine _corout;
    [Header("Page 2")]
    public GameObject Page2;
    public Transform arrowClickP2,
        arrowClickP22;
    private Vector3 arrowPos21, arrowPos22;

    public static int page;
    private int previousPage;

    public void Awake()
    {
        arrowOriPos = arrowClick.position;
        handOriPos = hand.position;
        handOriPos2 = hand2.position;
        arrowPos21 = arrowClickP2.position;
        arrowPos22 = arrowClickP22.position;

        page = 1;
        previousPage = 1;
    }

    private void OnEnable()
    {
        _corout = StartCoroutine("Animate");
    }

    private void OnDisable()
    {
        StopAllCoroutines();
        _corout = null;
    }

    private void Update()
    {
        if(page != previousPage)
        {
            previousPage = page;
            switch (page)
            {
                case 1:
                    Page1.SetActive(true);
                    Page2.SetActive(false);
                    break;
                case 2:
                    Page1.SetActive(false);
                    Page2.SetActive(true);
                    break;
            }
        }
    }

    IEnumerator Animate()
    {
        float v = 0, handTimer=0;
        Vector3 offset = new Vector3(0f, 0.2f, 0f);
        int dir = 0;

        while (true)
        {
            v += Time.deltaTime;
            handTimer += Time.deltaTime;
            if (v >= 1.5f)
            {
                v -= 1.5f;
                dir = dir == 0 ? 1 : 0;
            }
            switch (page)
            {
                case 1:
                    arrowClick.position = Vector3.Lerp(arrowOriPos + offset, arrowOriPos, v);
                    if (dir == 0)
                    {
                        hand2.position = Vector3.Lerp(handOriPos2, handOriPos2 + (offset), v);
                    }
                    else
                    {
                        hand2.position = Vector3.Lerp(handOriPos2, handOriPos2 - (offset), v);
                    }
                    var t = new Vector3(Mathf.Cos(Time.time) / 6f, Mathf.Sin(Time.time) / 6f, 0);
                    hand.position = handOriPos + t;
                    break;
                case 2:
                    if(v < 0.5f)
                        arrowClickP2.position = Vector3.Lerp(arrowPos21 + offset, arrowPos21, v*2);
                    else
                        arrowClickP2.position = Vector3.Lerp(arrowPos21 + offset, arrowPos21, (v-0.5f )* 2);
                    arrowClickP22.position = Vector3.Lerp(arrowPos22 + offset, arrowPos22, v);
                    break;
            }
            
        

            yield return null;
        }
    }
}
