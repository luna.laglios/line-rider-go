﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LR_languageSelector : MonoBehaviour {
    public Transform FR, ENG;
	
	// Update is called once per frame
	void Update () {
		if(LR_GLobal.language == LR_GLobal.FR)
        {
            transform.position = FR.position;
        }
        else if (LR_GLobal.language == LR_GLobal.ENG)
        {
            transform.position = ENG.position;
        }
	}
}
