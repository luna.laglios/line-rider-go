﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class MultiTrack : MonoBehaviour {
    public GameObject bezierPrefabs;
    private List<CurveMesh> beziers;
    private List<FileInfo> listFile;
    private List<Transform> listPoints;
    private int nb;
    private int maxTrack = 6;

    public Text debug;

	// Use this for initialization
	void Start () {
        beziers = new List<CurveMesh>();
        listFile = new List<FileInfo>();
        listPoints = new List<Transform>();
        getList();
        nb = listFile.Count;
        /*for(int i = 0; i < nb; i++)
        {
            GameObject b = Instantiate(bezierPrefabs, new Vector3(0, 0, 0), Quaternion.identity);
            CurveMesh bb = b.GetComponent<CurveMesh>();
            beziers.Add(bb);
            loadTrack(i);
        }
        */
        debug.text = "";
        for(int i = 0; i < nb && i< maxTrack; i++)
        {
            debug.text += "t " + i + "\n";
            loadPieceByPiece(i);
        }
        normaliseHeight();
        loadMegaTrack();
        debug.text += "Nb point : " + GameObject.FindObjectOfType<CurveMesh>().ControlPoints.Count;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void normaliseHeight()
    {
        if(listPoints.Count < 1)
        {
            return;
        }

        float min = listPoints[0].position.y;
        for(int i = 1; i < listPoints.Count; i++)
        {
            var l = listPoints[i].position.y;
            if (l < min)
            {
                min = l;
            }
        }
        if (min < 0f)
        {
            Vector3 off = new Vector3(0f,-min,0f);
            for (int i = 0; i < listPoints.Count; i++)
            {
                listPoints[i].Translate(off);
            }
        }
    }

    void loadMegaTrack()
    {
        GameObject b = Instantiate(bezierPrefabs, new Vector3(0, 0, 0), Quaternion.identity);
        CurveMesh bezier = b.GetComponent<CurveMesh>();
        bezier.ControlPoints = listPoints;
    }

    void loadPieceByPiece(int p)
    {
        string name = listFile[p].ToString();
        string location = Application.persistentDataPath + "/" + name;

        if (File.Exists(location))
        {
            var file = File.Open(location, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            var reader = new StreamReader(file);
            string tmp;
            tmp = reader.ReadLine();
            int nbcpt;
            if (System.Int32.TryParse(tmp, out nbcpt))
            {
                List<Vector3> newPos = new List<Vector3>();
                float x, y, z;
                for (int i = 0; i < nbcpt; i++)
                {
                    tmp = reader.ReadLine(); tmp = reader.ReadLine();
                    x = float.Parse(tmp);
                    tmp = reader.ReadLine(); tmp = reader.ReadLine();
                    y = float.Parse(tmp);
                    tmp = reader.ReadLine(); tmp = reader.ReadLine();
                    z = float.Parse(tmp);
                    Vector3 v = new Vector3(x, y, z);
                    newPos.Add(v);
                }
                float offx, offy, offz;
                offx = newPos[0].x;
                offy = newPos[0].y;
                offz = newPos[0].z;
                for(int i = 0; i < newPos.Count; i++)
                {
                    Vector3 point = newPos[i];
                    point.x -= offx;
                    point.y -= offy;
                    point.z -= offz;
                    newPos[i] = point;
                }
                if (p != 0)
                {
                    Vector3 lastPoint = listPoints[listPoints.Count - 1].position;
                    for(int i = 0; i < newPos.Count; i++)
                    {
                        Vector3 point = newPos[i];
                        point.x += lastPoint.x;
                        point.y += lastPoint.y;
                        point.z += lastPoint.z;
                        newPos[i] = point;
                    }
                }

                for(int i = 0; i < newPos.Count; i++)
                {
                    GameObject cp = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                    cp.transform.position = newPos[i];
                    cp.transform.rotation = Quaternion.identity;
                    LRCP options = cp.gameObject.AddComponent<LRCP>();
                    options.usesGravity = false;
                    options.erasable = true;
                    options.Bezier = null;
                    cp.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
                    Collider c = cp.GetComponent<Collider>();
                    if (c != null)
                    {
                        c.isTrigger = true;
                        (c as SphereCollider).radius = 1f;
                    }
                    listPoints.Add(cp.transform);
                }
            }
            reader.Close(); file.Close();
        }
    }





    void loadTrack(int p)
    {
        string name = listFile[p].ToString();
        string location = Application.persistentDataPath + "/" + name;

        if (File.Exists(location))
        {
            var file = File.Open(location, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            var reader = new StreamReader(file);
            string tmp;
            tmp = reader.ReadLine();
            int nbcpt;
            if (System.Int32.TryParse(tmp, out nbcpt))
            {
                List<Vector3> newPos = new List<Vector3>();
                float x, y, z;
                for (int i = 0; i < nbcpt; i++)
                {
                    tmp = reader.ReadLine(); tmp = reader.ReadLine();
                    x = float.Parse(tmp);
                    tmp = reader.ReadLine(); tmp = reader.ReadLine();
                    y = float.Parse(tmp);
                    tmp = reader.ReadLine(); tmp = reader.ReadLine();
                    z = float.Parse(tmp);
                    Vector3 v = new Vector3(x, y, z);
                    newPos.Add(v);
                }


                if (newPos.Count > 0)
                {
                    for (int i = 0; i < beziers[p].ControlPoints.Count; i++)
                    {
                        Destroy(beziers[p].ControlPoints[i].gameObject);
                    }
                    beziers[p].ControlPoints.Clear();

                    for (int i = 0; i < nbcpt; i++)
                    {
                        GameObject cp = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                        cp.transform.position = newPos[i];
                        cp.transform.rotation = Quaternion.identity;
                        LRCP options = cp.gameObject.AddComponent<LRCP>();
                        options.usesGravity = false;
                        options.erasable = true;
                        options.Bezier = beziers[p];
                        cp.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
                        Collider c = cp.GetComponent<Collider>();
                        if (c != null)
                        {
                            c.isTrigger = true;
                            (c as SphereCollider).radius = 1f;
                        }
                        beziers[p].ControlPoints.Add(cp.transform);
                    }
                }
            }
            reader.Close(); file.Close();
        }
    }

    void getList()
    {
        var info = new DirectoryInfo(Application.persistentDataPath);
        var list = info.GetFiles();
        if (list.Length > 0)
        {
            listFile.Clear();
            List<FileInfo> s1 = new List<FileInfo>();
            List<FileInfo> s2 = new List<FileInfo>();
            s1.Add(list[0]);
            for (int i = 1; i < list.Length; i++)
            {
                if (list[i].ToString().Length == s1[0].ToString().Length)
                {
                    s1.Add(list[i]);
                }
                else
                {
                    s2.Add(list[i]);
                }
            }
            for (int i = 0; i < s1.Count; i++)
            {
                listFile.Add(s1[i]);
            }
            for (int i = 0; i < s2.Count; i++)
            {
                listFile.Add(s2[i]);
            }
        }
    }
}
