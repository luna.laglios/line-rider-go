﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LRLevelManager : MonoBehaviour {
    public CurveMesh bezier;
    public GameObject world;
    public GameObject environment;
    private Vector3 envPos;
    private Vector3 oldScale;
    public Transform player;
    private Vector3 oldPos;
    private Vector3 oldRot;

    public static bool editor;
    public bool previousState;
    private float meshScaleEditor = 0.25f;
    private float meshScaleSlide = 2f;
    private Vector3 initialPlayerPosition;
    public LRTrain trainPrefab;
    private LRTrain trainObject;
    public Text deb;
    public GameObject postPrefab;
    private GameObject post;

	// Use this for initialization
	void Start () {
        initialPlayerPosition = player.position;
        oldScale = environment.transform.localScale;
        envPos = environment.transform.position;
        editor = true;
        if (!editor)
        {
            world.SetActive(false);
            previousState = false;
        }
        else
        {
            previousState = true;
        }
	}

    // Update is called once per frame
    void Update () {
        if (bezier == null)
        {
            bezier = GameObject.FindObjectOfType<CurveMesh>();
        }else

        if (previousState != editor)
        {
            if (!editor) //editor -> train
            {
                if (bezier.ControlPoints.Count > 0) {
                    world.SetActive(false);

                    Vector3 averagePos = Vector3.zero,
                        minPos = bezier.ControlPoints[0].position * 10f;
                    for (int i = 0; i < bezier.ControlPoints.Count; i++)
                    {
                        bezier.ControlPoints[i].position = bezier.ControlPoints[i].position * 10f;
                        bezier.MeshScale = meshScaleSlide;
                        averagePos += bezier.ControlPoints[i].position;
                        minPos = bezier.ControlPoints[i].position.y < minPos.y ? bezier.ControlPoints[i].position : minPos;
                    }
                    averagePos /= bezier.ControlPoints.Count;
                    environment.transform.position = minPos;
                    //environement rescale
                    environment.transform.localScale = new Vector3(6f, 5.5f, 6f);
                    oldPos = player.position;
                    oldRot = player.eulerAngles;

                    trainObject = Instantiate(trainPrefab, bezier.ControlPoints[0].position, Quaternion.identity);
                    trainObject.transform.LookAt(bezier.ControlPoints[1].position);
                    trainObject.running = true;
                    trainObject.bezier = bezier;
                    trainObject.editor = this;
                    trainObject.player = player;
                    trainObject.debug = false;


                    player.parent = trainObject.transform;
                    player.localPosition = new Vector3(0f, 1f, 0f);
                    player.localEulerAngles = Vector3.zero;
                    LR_GLobal.playerInTrain = true;
                    VRMovement op = player.gameObject.GetComponent<VRMovement>();
                    op.canMove = false;
                    op.canPick = false;
                    op.canUseBack = false;
                    LR_StressMusic music = FindObjectOfType<LR_StressMusic>();
                    if(music!=null) music.setCalm(true);

                    if (op.passwordMenu != null)
                    {
                        var go = op.passwordMenu.gameObject;
                        Destroy(go);
                        op.passwordMenu = null;
                    }


                    bezier.gameObject.transform.GetChild(0).gameObject.SetActive(false);
                    foreach (Transform t in bezier.ControlPoints)
                    {
                        t.gameObject.SetActive(false);
                    }

                    Vector3 postPos = bezier.ControlPoints[bezier.ControlPoints.Count - 1].position;
                    post = GameObject.Instantiate(postPrefab, postPos,Quaternion.identity);
                    post.transform.LookAt(bezier.ControlPoints[bezier.ControlPoints.Count-2].position);
                    post.transform.localPosition -= new Vector3(0, 2f, 0);
                }
                else
                {
                    editor = !editor;
                    if (deb != null)
                    {
                        deb.text += "no points";
                    }
                }
            }
            else //if train -> editor
            {
                world.SetActive(true);
                for (int i = 0; i < bezier.ControlPoints.Count; i++)
                {
                    bezier.ControlPoints[i].position = bezier.ControlPoints[i].position / 10f;
                    bezier.MeshScale = meshScaleEditor;
                }
                environment.transform.position = envPos;
                environment.transform.localScale = oldScale;
                player.position = oldPos;
                player.eulerAngles = oldRot;
                player.parent = null;
                LR_GLobal.playerInTrain = false;
                VRMovement op = player.gameObject.GetComponent<VRMovement>();
                op.canMove = true;
                op.canPick = true;
                op.canUseBack = true;
                LR_StressMusic music = FindObjectOfType<LR_StressMusic>();
                if (music != null) music.setCalm(true);

                if (trainObject != null)
                    Destroy(trainObject.gameObject);

                bezier.gameObject.transform.GetChild(0).gameObject.SetActive(true);
                foreach (Transform t in bezier.ControlPoints)
                {
                    t.gameObject.SetActive(true);
                }
                if (post != null)
                {
                    Destroy(post);
                }
            }
            previousState = editor;
        }
		
	}
}
