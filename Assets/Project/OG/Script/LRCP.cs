﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LRCP : OGPickable {
    public CurveMesh Bezier;
    private int count;

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        if (!picked && count > 0)
            count = 0;
    }

    public override void MainActionOnce()
    {
        if (count < 1)
            count++;
        else
        {
            Bezier.ControlPoints.Remove(this.transform);
            Destroy(this.gameObject);
        }
    }
}
