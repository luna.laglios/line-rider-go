﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LRMainMenuManager : MonoBehaviour {
    private bool inMenu;
    public Transform soundPanel;

    public Image passImage;
    public TMP_Text passText;

    private GameObject passwordMenuPrefabs;
    private GameObject passwordMenu;

    // Use this for initialization
    void Start () {
        inMenu = false;
        if (soundPanel.gameObject.activeSelf)
            soundPanel.gameObject.SetActive(false);
        var t = new LR_PasswordProtection();
        t.Start();
        passwordMenuPrefabs = Resources.Load("Prefabs/PasswordMenu", typeof(GameObject)) as GameObject;
    }
	
	// Update is called once per frame
	void Update () {
        if((OVRInput.GetUp(OVRInput.Button.Back,OVRInput.Controller.RTrackedRemote) ||
            OVRInput.GetUp(OVRInput.Button.Back, OVRInput.Controller.LTrackedRemote))){
            if(inMenu)
                hideSoundOption();
            else if(passwordMenu != null)
            {
                Destroy(passwordMenu);
            }
        }
        passImage.color = LR_GLobal.isPasswordOn ? Color.green : Color.red;
        passText.text = LR_GLobal.isPasswordOn ? "ON" : "OFF";
    }

    void LoadEditor()
    {
        if(!inMenu)
            SceneManager.LoadScene("Editor");
    }

    void LoadEternal()
    {
        if(!inMenu)
            SceneManager.LoadScene("Playground");
    }

    void displaySoundOption()
    {
        inMenu = true;
        soundPanel.gameObject.SetActive(true);
    }

    void hideSoundOption()
    {
        inMenu = false;
        soundPanel.gameObject.SetActive(false);
    }

    void changePassword()
    {
        LR_GLobal.switchPasswordMode();
    }

    void modifyPassword()
    {
        if(passwordMenu == null)
        {
            passwordMenu = Instantiate(passwordMenuPrefabs);
            passwordMenu.transform.parent = this.transform;
            passwordMenu.transform.localPosition = new Vector3(0, 0, -4.5f);
            passwordMenu.transform.localEulerAngles = Vector3.zero;
            passwordMenu.transform.localScale = new Vector3(1f, 1f, 1f);
        }
    }

    
}
