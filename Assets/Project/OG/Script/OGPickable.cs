﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OGPickable : MonoBehaviour {

    // Use this for initialization
    public bool hasRigibody = true;
    public bool usesGravity = true;
    public bool erasable = false;
    protected Rigidbody bod;
    protected Collider collid;
    public bool picked;
    private Transform oldParent;
	void Start () {
        if (hasRigibody) {
            bod = gameObject.AddComponent(typeof(Rigidbody)) as Rigidbody;
            bod.useGravity = usesGravity ? true : false;
        }
        picked = false;
        collid = GetComponent<Collider>();
        oldParent = transform.parent;
    }
	
	// Update is called once per frame
	void Update () {
        if (hasRigibody)
        {
            if(collid != null)
                collid.enabled = true;
            if (!picked && bod == null)
            {
                bod = gameObject.AddComponent(typeof(Rigidbody)) as Rigidbody;
                bod.useGravity = usesGravity ? true : false;
                Debug.Log(this.name + " has been dropped");
                transform.parent = oldParent;
            }
            else if (picked && bod!= null && bod.detectCollisions)
            {
                Destroy(bod);
                if(collid != null && this.name!= "Eraser")
                    collid.enabled = false ;
                //bod.detectCollisions = false;
                //bod.useGravity = false;
                Debug.Log(this.name + " has been picked up");
            }
        }
	}

    public void SetPick(bool value)
    {
        picked = value;
    }

    public virtual void MainActionOnce()
    {
        //Instantiate(this,this.transform.position, this.transform.rotation);
    }

    public virtual void MainActionContinuous()
    {

    }
}
